import { User } from './user';

export class LoginResponse {
    public user: User = null;
    public token: string = null;

    fromJSON(json: any) {
        if (!json) {
            return;
        }
        this.user = new User().fromJSON(json["user"]) || this.user;
        this.token = json["token"] || this.token;

        return this;
    }
}