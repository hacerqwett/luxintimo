export class Color {
	public id: number = -1;
	public name: string = null;
	public hex: string = null;

	fromJSON(json: any) {
		if (!json) {
			return;
		}
		this.id = json["id"] || this.id;
		this.name = json["name"] || this.name;
		this.hex = json["hex"] || this.hex;

		return this;
	}
}