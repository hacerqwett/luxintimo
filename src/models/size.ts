export class Size {
	public id: number = -1;
	public clothing_id: number = -1;
	public color_id: number = -1;
	public size: number = -1;
	public count: number = 0;

	fromJSON(json?: any) {
		if (!json) {
			return;
		}
		this.id = json["id"] || this.id;
		this.clothing_id = json["clothing_id"] || this.clothing_id;
		this.color_id = json["color_id"] || this.color_id;
		this.size = json["size"] || this.size;
		this.count = json["count"] || this.count;

		return this;
	}
}