export class Category {
    public id: number = -1;
    public name: string = null;
    public gender: number = null;

    fromJSON(json: any) {
        if (!json) {
            return;
        }
        this.id = json["id"] || this.id;
        this.name = json["name"] || this.name;
        this.gender = json["gender"] || this.gender;

        return this;
    }
}

export class SubCategory {
    public id: number = -1;
    public name: string = null;
    public category_id: number = null;

    fromJSON(json: any) {
        if (!json) {
            return;
        }
        this.id = json["id"] || this.id;
        this.name = json["name"] || this.name;
        this.category_id = json["category_id"] || this.category_id;

        return this;
    }
}

export class FullCategory {
    public category: Category = null;
    public subcategories: SubCategory[] = [];

    constructor(category: Category, subcategories: SubCategory[]) {
        this.category = category;
				this.subcategories = subcategories;
		}
}