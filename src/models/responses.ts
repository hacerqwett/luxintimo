import { Category, SubCategory } from './category';
import { Manufacturer } from './manufacturer';
import { Clothing, ClothingDetails } from './clothing';
import { Size } from './size';
import { Color } from './color';

export interface AllResponse {
	categories: Category[],
	subcategories: SubCategory[],
	manufacturers: Manufacturer[],
	clothing: Clothing[],
	sizes: Size[],
	colors: Color[],
}

export interface ClothingResponse {
	clothing: Clothing,
	details: ClothingDetails,
}