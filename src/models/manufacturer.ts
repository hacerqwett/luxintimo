export class Manufacturer {
    public id: number = -1;
    public name: string = null;

    fromJSON(json: any) {
        if (!json) {
            return;
        }
        this.id = json["id"] || this.id;
        this.name = json["name"] || this.name;

        return this;
    }
}