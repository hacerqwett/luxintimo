export class User {
    public id: number = -1;
    public email: string = null;
    public name: string = null;
    public address: string = null;
    public phone: string = null;
    public activated: boolean = false;
    public admin: boolean = false;
    public last_login: string = null;
    public date_joined: string = null;

    fromJSON(json: any) {
        if (!json) {
            return;
        }
        this.id = json["id"] || this.id;
        this.email = json["email"] || this.email;
        this.name = json["name"] || this.name;
        this.address = json["address"] || this.address;
        this.phone = json["phone"] || this.phone;
        this.activated = json["activated"] || this.activated;
        this.admin = json["admin"] || this.admin;
        this.last_login = json["last_login"] || this.last_login;
        this.date_joined = json["date_joined"] || this.date_joined;

        return this;
    }
}