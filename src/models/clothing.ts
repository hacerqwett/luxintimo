import { Image } from './image';
import { Manufacturer } from './manufacturer';
import { Category, SubCategory } from './category';
import { Size } from './size';

export class Clothing {
	public id: number = null;
	public name: string = null;
	public manufacturer_id: number = null;
	public subcategory_id: number = null;
	public status: number = null;
	public price: number = 0;
	public discount: number = 0;
	public discount_price: number = 0;
	public percentage: number = 0;
	public rating: number = 3.5;
	public images: Image[] = [];
	public sizes: Size[] = [];

	fromJSON(json: any) {
		if (!json) {
			return;
		}
		this.id = json["id"] || this.id;
		this.name = json["name"] || this.name;
		this.manufacturer_id = json["manufacturer_id"] || this.manufacturer_id;
		this.subcategory_id = json["subcategory_id"] || this.subcategory_id;
		this.status = json["status"] || this.status;
		this.price = json["price"] || this.price;
		this.discount = json["discount"] || this.discount;
		this.discount_price = json["discount_price"] || this.discount_price;
		this.percentage = Number(this.calcDiscountPercentage(this.price, this.discount).toFixed(0));
		this.rating = json["rating"] || this.rating;
		this.images = json['images'] ? json['images'].map(img => new Image().fromJSON(img)) : this.images;
		this.sizes = json['sizes'] ? json['sizes'].map(img => new Size().fromJSON(img)) : this.images;

		return this;
	}

	private calcDiscountPercentage(price, discount): number {
		return (price - discount) * 100 / price;
	}
}

export class ClothingDetails {
	public clothing_id: string = null;
	public description: string = null;

	fromJSON(json: any) {
		if (!json) {
			return;
		}
		this.clothing_id = json["clothing_id"] || this.clothing_id;
		this.description = json["description"] || this.description;

		return this;
	}
}

export class FullClothing extends Clothing {
	public manufacturer: Manufacturer = null;
	public category: Category = null;
	public subCategory: SubCategory = null;
	public details: ClothingDetails = null;

	fromJSON(json: any) {
		if (!json) {
			return;
		}
		super.fromJSON(json);

		this.images = json['images'] ? json['images'].map(img => new Image().fromJSON(img)) : this.images;
		this.manufacturer = new Manufacturer().fromJSON(json['manufacturer']);
		this.category = new Category().fromJSON(json['category']);
		this.subCategory = new SubCategory().fromJSON(json['subCategory']);
		this.details = new ClothingDetails().fromJSON(json['details']);

		return this;
	}
}