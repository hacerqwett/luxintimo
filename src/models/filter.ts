import { Gender, Size } from '@utils/constants';

export class FilterParams {
	category: number;
	gender: Gender;
	subcat: number[];
	sizes: Size[];
	colors: number[];
	brands: number[];
	sort: number;
	minPrice: number;
	maxPrice: number;

	constructor(params?: any) {
		this.category = null;
		this.gender = null;
		this.subcat = null;
		this.sizes = null;
		this.colors = null;
		this.brands = null;
		this.sort = null;
		this.minPrice = null;
		this.maxPrice = null;

		if (!params) {
			return this;
		}

		this.category = params['category'] ? params['category'] : this.category;
		this.gender =  params['gender'] ? params['gender'] : this.gender;
		this.subcat = params['subcat'] ? this.getArr(params['subcat']) : this.subcat;
		this.sizes = params['sizes'] ? this.getArr(params['sizes']) : this.sizes;
		this.colors = params['colors'] ? this.getArr(params['colors']) : this.colors;
		this.brands = params['brands'] ? this.getArr(params['brands']) : this.brands;
		this.sort = params['sort'] ? params['sort'] : this.sort;
		this.minPrice = params['minPrice'] ? params['minPrice'] : this.minPrice;
		this.maxPrice = params['maxPrice'] ? params['maxPrice'] : this.maxPrice;
	}

	private getArr(obj) {
		if (Array.isArray(obj)) {
			return obj
		}
		return [Number(obj)];
	}
}