import { Color } from './color';
import { environment } from '@env/environment';
import { Aspect } from '@utils/constants';

export enum ImageSize {
	Small,
	Medium,
	Large
}

export class Image {
	public id: number = null;
	public name: string = null;
	public uri: string = null;
	public clothing_id: number = null;
	public color_id: number = null;
	public aspect: number = null;
	public color: Color = null;
	public srcset: string = null;

	fromJSON(json: any) {
		if (!json) {
			return;
		}
		this.id = json["id"] || this.id;
		this.name = json["name"] || this.name;
		if (this.name) {
			this.uri = environment.imageWebServiceUri + '/' + this.name;
		}
		this.clothing_id = json["clothing_id"] || this.clothing_id;
		this.aspect = json["aspect"] || this.aspect;
		this.color_id = json["color_id"] || this.color_id;
		this.color = json["color"] || this.color;

		return this;
	}

	setSrcSet(size: ImageSize) {
		this.srcset = this.genSrcSet(size);
	}

	genSrcSet(size: ImageSize) {
		if (!this.uri) {
			return;
		}

		if (this.aspect === Aspect.AspectRect75) {
			switch (size) {
				case ImageSize.Large:
					return `${environment.imageWebServiceUri}/900x1200-${this.name} 900w,
					 ${environment.imageWebServiceUri}/1080x1440-${this.name} 1080w,
					 ${environment.imageWebServiceUri}/1296x1728-${this.name} 1296w,
					 ${environment.imageWebServiceUri}/1512x2016-${this.name} 1512w,
					 ${environment.imageWebServiceUri}/1728x2304-${this.name} 1728w,
					${environment.imageWebServiceUri}/2049x2732-${this.name} 2048w`;
				case ImageSize.Medium:
					return `${environment.imageWebServiceUri}/540x720-${this.name} 1296w,
					 ${environment.imageWebServiceUri}/720x960-${this.name} 2048w`;
			}
		} else {
			switch (size) {
				case ImageSize.Large:
					return `${environment.imageWebServiceUri}/900-${this.name} 900w,
					 ${environment.imageWebServiceUri}/1080-${this.name} 1080w,
					 ${environment.imageWebServiceUri}/1296-${this.name} 1296w,
					 ${environment.imageWebServiceUri}/1512-${this.name} 1512w,
					 ${environment.imageWebServiceUri}/1728-${this.name} 1728w,
					${environment.imageWebServiceUri}/2048-${this.name} 2048w`;
				case ImageSize.Medium:
					return `${environment.imageWebServiceUri}/540-${this.name} 1296w,
					 ${environment.imageWebServiceUri}/720-${this.name} 2048w`;
			}
		}
	}
}