import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Auth } from '@utils/auth';
import { catchError } from 'rxjs/operators'
import { Subject, Observable, throwError } from 'rxjs';
import { NotificationService } from './notification.service';
import { Router } from '@angular/router';

export interface ErrorResponse {
	status: number,
	message: string
}

export interface SuccessResponse {
	summary: string,
	detail: string
}

export class BaseWebService {
	public static errorSubject = new Subject<ErrorResponse>();
	public static successSubject = new Subject<SuccessResponse>();

	constructor(protected http: HttpClient, protected notificationService: NotificationService) {

	}

	protected async apiGet(url: string, params?: any): Promise<any> {
		let headers = this.getHeaders();

		return this.http.get(url, { headers, params }).pipe(catchError(err => this.handleError(err))).toPromise();
	}

	protected async apiPost(url: string, params?: any): Promise<any> {
		let headers = this.getHeaders();

		return this.http.post(url, params, { headers }).pipe(catchError(err => this.handleError(err))).toPromise();
	}

	protected async apiPatch(url: string, params?: any): Promise<any> {
		let headers = this.getHeaders();

		return this.http.patch(url, params, { headers }).pipe(catchError(err => this.handleError(err))).toPromise();
	}

	protected async apiDelete(url: string): Promise<any> {
		let headers = this.getHeaders();

		return this.http.delete(url, { headers }).pipe(catchError(err => this.handleError(err))).toPromise();
	}

	protected getHeaders() {
		let headers = new HttpHeaders();
		headers = headers.set('Content-Type', 'application/json; charset=utf-8');
		headers = this.getHeadersWithToken(headers);
		return headers;
	}

	protected handleError(res): Observable<never> {
		let err = res.err;
		this.notificationService.errorSubject.next(err);
		return throwError(res);
	}

	private getHeadersWithToken(headers): HttpHeaders {
		let token = Auth.getJWT();
		if (token && !Auth.validJWT(token)) {
			Auth.clear();
			return headers;
		}

		return headers.set('Authorization', 'Bearer ' + token);
	}
}