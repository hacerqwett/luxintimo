import { BaseWebService } from './base-web.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { LoginResponse } from 'src/models/login-response';
import { AllResponse, ClothingResponse } from '@models/responses';
import { NotificationService } from './notification.service';
import { Clothing } from '@models/clothing';

@Injectable()
export class UserWebService extends BaseWebService {
	private webServiceUri: string = environment.webServiceUri

	constructor(protected http: HttpClient, protected notificationService: NotificationService) {
		super(http, notificationService);
	}

	public async login(params: any): Promise<LoginResponse> {
		let url = this.webServiceUri + "/login";
		let res = await this.apiPost(url, params);
		this.notificationService.successSubject.next({ summary: 'Logged in', detail: 'Successfully logged in!' });
		return new LoginResponse().fromJSON(res);
	}
}

@Injectable()
export class ClothingWebService extends BaseWebService {
	private webServiceUri: string = environment.webServiceUri

	constructor(protected http: HttpClient, protected notificationService: NotificationService) {
		super(http, notificationService);
	}

	public async getAll(): Promise<AllResponse> {
		let url = this.webServiceUri + "/all";
		let res = await this.apiGet(url);
		return res;
	}

	public async getClothing(id: any): Promise<ClothingResponse> {
		let url = this.webServiceUri + "/clothing/" + id;
		let res = await this.apiGet(url);
		return res;
	}

	public async getCollection(index: number, params?): Promise<Clothing[]> {
		let url = this.webServiceUri + "/collection/" + index;
		let res = await this.apiGet(url, params);
		return res;
	}
}

@Injectable()
export class AdminWebService extends BaseWebService {
	private webServiceUri: string = environment.webServiceUri

	constructor(protected http: HttpClient, protected notificationService: NotificationService) {
		super(http, notificationService);
	}

	// Category
	public async createCategory(category: any): Promise<number> {
		let url = this.webServiceUri + `/category`;
		let res = await this.apiPost(url, category);
		this.notificationService.successSubject.next({ summary: 'Created Category', detail: 'Successfully created Category!' });
		return Number(res);
	}

	public async updateCategory(category: any): Promise<void> {
		let url = this.webServiceUri + `/category/${category.id}`;
		await this.apiPatch(url, category);
		this.notificationService.successSubject.next({ summary: 'Created Category', detail: 'Successfully created Category!' });
	}

	public async deleteCategory(id: number): Promise<void> {
		let url = this.webServiceUri + `/category/${id}`;
		await this.apiDelete(url);
		this.notificationService.successSubject.next({ summary: 'Created Subcategory', detail: 'Successfully created Subcategory!' });
	}

	// SubCategory
	public async createSubCategory(subcategory: any): Promise<number> {
		let url = this.webServiceUri + `/subcategory`;
		let res = await this.apiPost(url, subcategory);
		this.notificationService.successSubject.next({ summary: 'Created Subcategory', detail: 'Successfully created Subcategory!' });
		return Number(res);
	}

	public async updateSubCategory(subcategory: any): Promise<void> {
		let url = this.webServiceUri + `/subcategory/${subcategory.id}`;
		await this.apiPatch(url, subcategory);
		this.notificationService.successSubject.next({ summary: 'Created Subcategory', detail: 'Successfully created Subcategory!' });
	}

	public async deleteSubCategory(id: number): Promise<void> {
		let url = this.webServiceUri + `/subcategory/${id}`;
		await this.apiDelete(url);
		this.notificationService.successSubject.next({ summary: 'Created Subcategory', detail: 'Successfully created Subcategory!' });
	}

	// Manufacturer
	public async createManufacturer(manufacturer: any): Promise<number> {
		let url = this.webServiceUri + `/manufacturer`;
		let res = await this.apiPost(url, manufacturer);
		this.notificationService.successSubject.next({ summary: 'Created Manufacturer', detail: 'Successfully created Manufacturer!' });
		return Number(res);
	}

	public async updateManufacturer(manufacturer: any): Promise<void> {
		let url = this.webServiceUri + `/manufacturer/${manufacturer.id}`;
		await this.apiPatch(url, manufacturer);
		this.notificationService.successSubject.next({ summary: 'Created Manufacturer', detail: 'Successfully created Manufacturer!' });
	}

	public async deleteManufacturer(id: number): Promise<void> {
		let url = this.webServiceUri + `/manufacturer/${id}`;
		await this.apiDelete(url);
		this.notificationService.successSubject.next({ summary: 'Created Manufacturer', detail: 'Successfully created Manufacturer!' });
	}

	// Clothing
	public async createClothing(clothing: any): Promise<number> {
		let url = this.webServiceUri + `/clothing`;
		let res = await this.apiPost(url, clothing);
		this.notificationService.successSubject.next({ summary: 'Created Manufacturer', detail: 'Successfully created Manufacturer!' });
		return Number(res);
	}

	public async updateClothing(clothing: any): Promise<void> {
		let url = this.webServiceUri + `/clothing/${clothing.id}`;
		await this.apiPatch(url, clothing);
		this.notificationService.successSubject.next({ summary: 'Created Manufacturer', detail: 'Successfully created Manufacturer!' });
	}

	public async deleteClothing(id: number): Promise<void> {
		let url = this.webServiceUri + `/clothing/${id}`;
		await this.apiDelete(url);
		this.notificationService.successSubject.next({ summary: 'Created Manufacturer', detail: 'Successfully created Manufacturer!' });
	}

	// Clothing Size
	public async createSize(size: any): Promise<number> {
		let url = this.webServiceUri + `/size`;
		let res = await this.apiPost(url, size);
		return Number(res);
	}

	public async updateSize(size: any): Promise<void> {
		let url = this.webServiceUri + `/size/${size.id}`;
		await this.apiPatch(url, size);
	}

	public async deleteSize(id: number): Promise<void> {
		let url = this.webServiceUri + `/size/${id}`;
		await this.apiDelete(url);
	}

	// Color
	public async createColor(color: any): Promise<number> {
		let url = this.webServiceUri + `/color`;
		let res = await this.apiPost(url, color);
		this.notificationService.successSubject.next({ summary: 'Created Color', detail: 'Successfully created Color!' });
		return Number(res);
	}

	public async updateColor(color: any): Promise<void> {
		let url = this.webServiceUri + `/color/${color.id}`;
		await this.apiPatch(url, color);
		this.notificationService.successSubject.next({ summary: 'Updated Color', detail: 'Successfully updated Color!' });
	}

	public async deleteColor(id: number): Promise<void> {
		let url = this.webServiceUri + `/color/${id}`;
		await this.apiDelete(url);
		this.notificationService.successSubject.next({ summary: 'Deleted Color', detail: 'Successfully deleted Color!' });
	}

	// Clothing Image
	public async updateImage(id: number, params: any): Promise<void> {
		let url = this.webServiceUri + `/image/${id}`;
		await this.apiPatch(url, params);
		this.notificationService.successSubject.next({ summary: 'Updated Clothing Image', detail: 'Successfully created Clothing Image!' });
	}

	public async deleteImage(id: number): Promise<void> {
		let url = this.webServiceUri + `/image/${id}`;
		await this.apiDelete(url);
		this.notificationService.successSubject.next({ summary: 'Deleted Clothing Image', detail: 'Successfully deleted Clothing Image!' });
	}

	// File
	public async deleteFile(id: number): Promise<void> {
		let url = this.webServiceUri + `/file/${id}`;
		await this.apiDelete(url);
		this.notificationService.successSubject.next({ summary: 'Created Manufacturer', detail: 'Successfully created Manufacturer!' });
	}
}