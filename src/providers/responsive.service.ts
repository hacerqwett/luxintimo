import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Resolution } from '@utils/constants';

@Injectable()
export class ResponsiveService {
	public screenWidth = new ReplaySubject<Resolution>(1);

	constructor() {
		this.checkWidth();
	}

	public checkWidth() {
		var width = window.innerWidth;
		if (width <= 575) {
			this.screenWidth.next(Resolution.ExtraSmall);
		} else if (width > 575 && width <= 768) {
			this.screenWidth.next(Resolution.Small);
		} else if (width > 768 && width <= 992) {
			this.screenWidth.next(Resolution.Medium);
		} else {
			this.screenWidth.next(Resolution.Large);
		}
	}
}
