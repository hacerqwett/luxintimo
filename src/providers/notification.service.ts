import { Subject } from 'rxjs';

export interface ErrorResponse {
	status: number,
	message: string
}

export interface SuccessResponse {
	summary: string,
	detail: string
}

export class NotificationService {
	public errorSubject = new Subject<ErrorResponse>();
	public successSubject = new Subject<SuccessResponse>();

	constructor() {

	}
}