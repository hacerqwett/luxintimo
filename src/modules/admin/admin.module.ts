import { NgModule } from '@angular/core';
import { AdminWebService } from '@providers/web.service';
import { AdminService } from './admin.service';
import { ClothingComponent } from './clothing/clothing.component';
import { AdminRoutingModule } from './admin-routing.module';
import { CommonModule } from '@angular/common';
import { AdminCommonModule } from './common/admin.common.module';
import { ColorPickerService } from './common/color-picker/color-picker.service';
import { UploaderService } from './common/uploader/uploader.service'
import { LuxCommonModule } from '@modules/app/common/lux.common.module';
import 'hammerjs';

@NgModule({
	declarations: [
		ClothingComponent,
	],
	imports: [
		CommonModule,
		LuxCommonModule,
		AdminRoutingModule,
		AdminCommonModule,
	],
	providers: [
		AdminWebService,
		AdminService,
		ColorPickerService,
		UploaderService
	]
})
export class AdminModule { }
