import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClothingComponent } from './clothing/clothing.component';

const routes: Routes = [
	{ path: '', redirectTo: 'clothing', pathMatch: 'full' },
	{ path: 'clothing', component: ClothingComponent },
	{ path: 'clothing/:id', component: ClothingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
