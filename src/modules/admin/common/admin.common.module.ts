import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { NgxUploaderModule } from 'ngx-uploader';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { CreateCategoryModalComponent } from './modals/create-category/create-category.component';
import { CreateSubCategoryModalComponent } from './modals/create-subcategory/create-subcategory.component';
import { CreateManufacturerModalComponent } from './modals/create-manufacturer/create-manufacturer.component';
import { ColorPickerModalComponent } from './modals/color-picker-modal/color-picker-modal.component';
import { UploaderComponent } from './uploader/uploader.component';
import { SizeModalComponent } from './modals/size-modal/size-modal.component';
import { CropperComponent } from './cropper/cropper.component';
import { ClothingTopComponent } from '../clothing/clothing-top/clothing-top.component';
import { ClothingFormComponent } from '../clothing/clothing-form/clothing-form.component';
import { MatInputModule, MatSelectModule, MatButtonModule, MatDialogModule, MatFormFieldModule, MatRadioModule, MatIconModule, MatCardModule, MatToolbarModule, MatBadgeModule, MatProgressSpinnerModule, MatSliderModule, MatSlideToggleModule, MatCheckboxModule, MatTabsModule, MatAutocompleteModule } from '@angular/material'
import { LuxSpinnerComponent } from '@modules/app/common/lux-spinner/lux-spinner.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LuxCommonModule } from '@modules/app/common/lux.common.module';


@NgModule({
	declarations: [
		UploaderComponent,
		CreateCategoryModalComponent,
		CreateSubCategoryModalComponent,
		CreateManufacturerModalComponent,
		ColorPickerComponent,
		CropperComponent,
		ColorPickerModalComponent,
		SizeModalComponent,
		ClothingFormComponent,
		ClothingTopComponent,
		LuxSpinnerComponent,
	],
	imports: [
		CommonModule,
		LuxCommonModule,
		ReactiveFormsModule,
		NgxUploaderModule,
		MatInputModule,
		MatSelectModule,
		MatButtonModule,
		MatDialogModule,
		MatFormFieldModule,
		MatRadioModule,
		MatIconModule,
		MatCardModule,
		MatToolbarModule,
		MatBadgeModule,
		MatProgressSpinnerModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatCheckboxModule,
		MatTabsModule,
		MatAutocompleteModule,
	],
	exports: [
		NgxUploaderModule,
		UploaderComponent,
		CreateCategoryModalComponent,
		CreateSubCategoryModalComponent,
		CreateManufacturerModalComponent,
		ColorPickerComponent,
		CropperComponent,
		ColorPickerModalComponent,
		SizeModalComponent,
		ClothingFormComponent,
		ClothingTopComponent,
		MatInputModule,
		MatSelectModule,
		MatButtonModule,
		MatDialogModule,
		MatFormFieldModule,
		MatRadioModule,
		MatIconModule,
		MatCardModule,
		MatToolbarModule,
		MatBadgeModule,
		MatProgressSpinnerModule,
		MatSliderModule,
		MatSlideToggleModule,
		MatCheckboxModule,
		MatTabsModule,
		MatAutocompleteModule,
		LuxSpinnerComponent,
	],
	entryComponents: [
		CreateManufacturerModalComponent,
		CreateCategoryModalComponent,
		CreateSubCategoryModalComponent,
		ColorPickerModalComponent,
		SizeModalComponent,
	]
})
export class AdminCommonModule { }
