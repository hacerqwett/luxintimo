import { Injectable, Renderer2 } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class ColorPickerService {
	public outputColorSubject = new ReplaySubject<string>(1);
	public inputColorSubject = new ReplaySubject<string>(1);
}

export class ColorPickerInstance {
	public colorPickerStyleClass = 'color-picker-style';
	public colorPickerItemClass = 'color-picker-item';

	public colorPickerItemElements: HTMLElement[];
	public styledElements: HTMLElement[];

	constructor(
		private _renderer2: Renderer2,
		private container: any,
	) {
	}

	public setStyledElements() {
		this.styledElements = this.container.getElementsByClassName(this.colorPickerStyleClass);
	}

	public setColorItemElements() {
		this.colorPickerItemElements = this.container.getElementsByClassName(this.colorPickerItemClass);
	}

	public addStyledElement(index: number) {
		let el = this.colorPickerItemElements[index];
		el.classList.add(this.colorPickerStyleClass);
		this.setStyledElements();
	}

	public removeStyledElement(index: number) {
		let el = this.colorPickerItemElements[index];
		el.classList.remove(this.colorPickerStyleClass);
		this.setStyledElements();
	}

	public setElementBackgroundColor(el: HTMLElement, hex: string) {
		this._renderer2.setStyle(el, 'background-color', hex);
	}

	public setElementColor(el, hex) {
		this._renderer2.setStyle(el, 'color', hex);
	}

	public calcElementColor(hex: string): string {
		let r = parseInt(hex.substring(1, 2), 16);
		let g = parseInt(hex.substring(3, 4), 16);
		let b = parseInt(hex.substring(5, 6), 16);
		let res = r * 0.299 + g * 0.587 + b * 0.114;
		return res > 10 ? '#333333' : '#ffffff';
	}

	public lastColorHex: string;
	public setElementStyle(hex: string) {
		if (!this.styledElements) {
			return;
		}
		let colorHex = this.calcElementColor(hex);
		for (let el of this.styledElements) {
			this.setElementBackgroundColor(el, hex);
			if (colorHex !== this.lastColorHex) {
				this.setElementColor(el, colorHex);
			}
		}
		this.lastColorHex = colorHex;
	}
}