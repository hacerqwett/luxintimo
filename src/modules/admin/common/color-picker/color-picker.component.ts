import { Component, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import ReinventedColorWheel from "ng-reinvented-color-wheel";
import { ColorPickerService } from './color-picker.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'color-picker',
	templateUrl: './color-picker.component.html',
	styleUrls: [ './color-picker.component.css' ],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColorPickerComponent {

	private colorWheel: ReinventedColorWheel;

	private inputSub: Subscription;
	private skip: boolean = false;

	ngAfterViewInit(): void {
		this.ref.detach();

		let colorPickerElement = this.container.nativeElement.getElementsByClassName('color-picker')[0];
		let width = colorPickerElement.getBoundingClientRect().width.toFixed();
		this.colorWheel = new ReinventedColorWheel({
			appendTo: colorPickerElement,

			hex: "#ff0000",

			wheelDiameter: width,
			wheelThickness: 20,
			handleDiameter: 16,
			wheelReflectsSaturation: false,

			onChange: (color) => {
				if (this.skip) {
					this.skip = false;
					return;
				}
				this.colorPickerService.outputColorSubject.next(color.hex);
			}
		});

		this.inputSub = this.colorPickerService.inputColorSubject.subscribe(hex => {
			this.skip = true;
			this.colorWheel.hex = hex;
		});
	}

	constructor(
		private colorPickerService: ColorPickerService,
		private container: ElementRef,
		private ref: ChangeDetectorRef
	) {
	}

	ngOnDestroy(): void {
		this.colorWheel.destroy();
		this.inputSub.unsubscribe();
	}
}
