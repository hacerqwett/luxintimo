import { Component, Inject } from '@angular/core';
import { AdminWebService } from '@providers/web.service';
import { AppService } from '@modules/app/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FullCategory, SubCategory, Category } from '@models/category';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AdminService } from '@modules/admin/admin.service';

@Component({
  selector: 'create-subcategory-modal',
  templateUrl: './create-subcategory.component.html',
})
export class CreateSubCategoryModalComponent {

	public categories: Category[] = [];
	public subCategories: SubCategory[] = [];
	public fullCategories: FullCategory[] = [];

  public createSubCategoryForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    category_id: new FormControl(null, Validators.required),
  });

  constructor(
    private adminWebService: AdminWebService,
		private appService: AppService,
		private adminService: AdminService,
		public dialogRef: MatDialogRef<CreateSubCategoryModalComponent>,
		@Inject(MAT_DIALOG_DATA) private data: any
  ) {
		this.appService.categorySubject.subscribe((categories) => {
			this.categories = categories;
			this.createSubCategoryForm.patchValue({ category_id: data.category_id })
		})
		this.appService.fullCategorySubject.subscribe((fullCategories) => {
			this.fullCategories = fullCategories;
		})
	}
	
  cancel(): void {
    this.dialogRef.close();
	}
	
  public async createSubCategory() {
		let resID = await this.adminWebService.createSubCategory(this.createSubCategoryForm.value);
		let subCategory: SubCategory = await this.adminService.createSubCategory({
			id: resID,
			...this.createSubCategoryForm.value
		});

		this.dialogRef.close(subCategory);
  }
}
