import { Component, EventEmitter, Output } from '@angular/core';
import { AdminWebService } from '@providers/web.service';
import { AppService } from '@modules/app/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Manufacturer } from '@models/manufacturer';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'create-manufacturer-modal',
  templateUrl: './create-manufacturer.component.html',
})
export class CreateManufacturerModalComponent {

  public createManufacturerForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
  });

  public manufacturers: Manufacturer[] = [];
  public manufacturerSub: Subscription;

  constructor(
    private adminWebService: AdminWebService,
		private appService: AppService,
    public dialogRef: MatDialogRef<CreateManufacturerModalComponent>,
  ) {
    this.manufacturerSub = this.appService.manufacturerSubject.subscribe((manufacturers) => {
			this.manufacturers = manufacturers;
		})
	}
	
  cancel(): void {
    this.dialogRef.close();
  }
  
  public async createManufacturer() {
		let resID = await this.adminWebService.createManufacturer(this.createManufacturerForm.value);
		let manufacturer: Manufacturer = new Manufacturer().fromJSON({
			id: resID,
			...this.createManufacturerForm.value
		});
		this.manufacturers.push(manufacturer);
		this.appService.manufacturerSubject.next(this.manufacturers);
		
		this.dialogRef.close(manufacturer);
	}
	
	ngOnDestroy(): void {
		this.manufacturerSub.unsubscribe();
	}
}
