import { Component } from '@angular/core';
import { GENDERS } from '@utils/constants'
import { AdminWebService } from '@providers/web.service';
import { AppService } from '@modules/app/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Category, FullCategory } from '@models/category';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material';
import { AdminService } from '@modules/admin/admin.service';

@Component({
  selector: 'create-category-modal',
  templateUrl: './create-category.component.html',
})
export class CreateCategoryModalComponent {

	public genders = GENDERS;

	public categories: Category[] = [];
	public categorySub: Subscription;

	public fullCategories: FullCategory[] = [];
	public fullCategorySub: Subscription;

  public createCategoryForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    gender: new FormControl('', Validators.required),
  });

  constructor(
    private adminWebService: AdminWebService,
		private appService: AppService,
		private adminService: AdminService,
		public dialogRef: MatDialogRef<CreateCategoryModalComponent>,
  ) {
		this.categorySub = this.appService.categorySubject.subscribe((categories) => {
			this.categories = categories;
		});
	}

  cancel(): void {
    this.dialogRef.close();
  }
  
  public async createCategory() {
		let resID = await this.adminWebService.createCategory(this.createCategoryForm.value);
		let category: Category = await this.adminService.createCategory({
			id: resID,
			...this.createCategoryForm.value
		});

    this.dialogRef.close(category);
	}
	
	ngOnDestroy(): void {
		this.categorySub.unsubscribe();
	}
}
