import { Component, Inject, Renderer2, ElementRef } from '@angular/core';
import { AdminWebService } from '@providers/web.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Color } from '@models/color';
import { SIZES } from '@utils/constants';
import { Size } from '@models/size';
import { ColorPickerInstance } from '../../color-picker/color-picker.service';

interface SizeModalData {
	clothing_id: number,
	colors: Color[],
	sizes: Size[],
}

interface ColorSizeMap {
	[color: string]: Size[];
}

@Component({
	selector: 'size-modal',
	templateUrl: './size-modal.component.html',
})
export class SizeModalComponent {

	public clothing_id: number = -1;

	public colors: Color[] = [];
	public chosenColor: Color = null;

	public sizes = SIZES.map(s => { return {...s} });
	public chosenSize: Size = null;

	public data_sizes: Size[];

	public color_sizes: ColorSizeMap = {};

	public colorPickerInstance: ColorPickerInstance;

	public count: number = 0;
	public currCount: number = 0;
	public lastCount: number = 0;

	constructor(
		private adminWebService: AdminWebService,
		public dialogRef: MatDialogRef<SizeModalComponent>,
		private _renderer2: Renderer2,
		private container: ElementRef,
		@Inject(MAT_DIALOG_DATA) private data: SizeModalData,
	) {
		this.colorPickerInstance = new ColorPickerInstance(this._renderer2, this.container.nativeElement);
		this.clothing_id = data.clothing_id;
		this.colors = JSON.parse(JSON.stringify(data.colors));
		this.colors.forEach(c => {
			this.color_sizes[c.id] = [];
			this.sizes.forEach(s => {
				this.color_sizes[c.id].push(new Size());
			});
		});
		this.data_sizes = data.sizes;
		if (!data.sizes || data.sizes.length === 0) {
			return;
		}
		for (let i = 0; i < data.sizes.length; ++i) {
			let s = data.sizes[i];
			this.color_sizes[s.color_id][s.size - 1] = new Size().fromJSON(s);
		}
	}

	ngAfterViewInit(): void {
		this.colorPickerInstance.setColorItemElements();
		if (this.colors.length > 0) {
			this.onChooseColor(this.colors[0]);
		}
	}

	public async setSize() {
		if (!this.currCount || this.currCount === this.lastCount) {
			return;
		}

		if (this.currCount === 0 && this.lastCount > 0) {
			let id = this.color_sizes[this.chosenColor.id][this.chosenSize.id - 1].id;
			await this.adminWebService.deleteSize(id);
			let index = this.data_sizes.findIndex(s => s.id === id);
			this.data_sizes.splice(index, 1);
			return;
		}

		let params = { clothing_id: this.clothing_id, color_id: this.chosenColor.id, size: this.chosenSize.id, count: this.currCount };
		if (this.currCount > 0 && this.lastCount === 0) {
			let id = await this.adminWebService.createSize(params);
			params['id'] = id;
			this.color_sizes[this.chosenColor.id][this.chosenSize.id - 1].id = id;
			this.data_sizes.push(new Size().fromJSON(params));
			return;
		}

		params['id'] = this.color_sizes[this.chosenColor.id][this.chosenSize.id - 1].id;
		await this.adminWebService.updateSize(params)
		let index = this.data_sizes.findIndex(s => s.id === params['id']);
		this.data_sizes[index] = new Size().fromJSON(params);
	}

	public onChooseColor(color) {
		this.chosenSize = this.choose(this.chosenSize, null);
		this.chosenColor = this.choose(this.chosenColor, color);

		this.setChosenElementColorStyle();
	}

	public onChooseSize(size) {
		if (this.chosenSize) {
			this.setSize();
		}
		this.chosenSize = this.choose(this.chosenSize, size);
		if (this.chosenSize) {
			this.count = this.color_sizes[this.chosenColor.id][this.chosenSize.id - 1].count;
		}
		this.currCount = this.count;
		this.lastCount = this.count;
	}

	public onCountChange({ value }) {
		this.currCount = value;
		this.color_sizes[this.chosenColor.id][this.chosenSize.id - 1].count = value;
	}

	private choose(last, chosen) {
		if (last) {
			last['selected'] = false;

			if (last === chosen) {
				last['selected'] = false;
				return null;
			}
		}

		if (chosen) {
			chosen['selected'] = true;
		}
		return chosen;
	}

	private setChosenElementColorStyle() {
		if (this.chosenColor['styled']) {
			return;
		}

		let index = this.colors.findIndex(c => c.id === this.chosenColor.id);
		this.colorPickerInstance.addStyledElement(index);
		let hex = this.colorPickerInstance.calcElementColor(this.chosenColor.hex);
		this.colorPickerInstance.setElementColor(this.colorPickerInstance.colorPickerItemElements[index], hex);
		this.chosenColor['styled'] = true;
	}

	close(): void {
		this.dialogRef.close();
	}

	ngOnDestroy(): void {
		this.setSize();
	}
}
