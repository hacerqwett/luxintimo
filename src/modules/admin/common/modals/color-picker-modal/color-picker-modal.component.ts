import { Component, Renderer2, ElementRef, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AdminWebService } from '@providers/web.service';
import { AppService } from '@modules/app/app.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Color } from '@models/color';
import { ColorPickerService, ColorPickerInstance } from '../../color-picker/color-picker.service';
import { Image } from '@models/image';
import { AdminService } from '@modules/admin/admin.service';
import { Utils } from '@utils/utils';

@Component({
	selector: 'color-picker-modal',
	templateUrl: './color-picker-modal.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColorPickerModalComponent {

	public colorForm: FormGroup = new FormGroup({
		name: new FormControl({ value: '', disabled: true }, Validators.required),
	});

	public colors: Color[] = [];
	private colorSub: Subscription;

	public uniqueColors: Color[];

	public selectingColor: boolean = false;
	public creatingColor: boolean = false;
	public editingColor: boolean = false;
	public deletingColor: boolean = false;

	private chosenColor: Color;
	private chosenElementHex: string;

	public colorPickerInstance: ColorPickerInstance;

	private colorPickerSub: Subscription;

	private image: Image;

	constructor(
		private adminWebService: AdminWebService,
		private appService: AppService,
		private adminService: AdminService,
		private colorPickerService: ColorPickerService,
		private _renderer2: Renderer2,
		private container: ElementRef,
		private dialogRef: MatDialogRef<ColorPickerModalComponent>,
		private cdRef: ChangeDetectorRef,
		@Inject(MAT_DIALOG_DATA) private data: any
	) {
		this.colorPickerInstance = new ColorPickerInstance(this._renderer2, this.container.nativeElement);
		this.image = data.image;
		this.colorSub = this.appService.colorSubject.subscribe((colors) => {
			this.colors = colors;
			this.uniqueColors = Utils.getOnlyUnique(this.colors, 'name');
		});
		this.colorPickerSub = this.colorPickerService.outputColorSubject.subscribe((hex) => {
			this.chosenElementHex = hex;
			this.colorPickerInstance.setElementStyle(hex);
			if (this.chosenColor && this.chosenColor['selected']) {
				let index = this.colors.findIndex(c => c.id === this.chosenColor.id);
				this.editColor(this.colors[index]);
				this.cdRef.detectChanges();
			}
		});
	}

	ngAfterViewInit(): void {
		this.colorPickerInstance.setStyledElements();
		this.colorPickerInstance.setColorItemElements();
		if (this.image.color) {
			this.onChooseColor(this.image.color);
		}
		this.cdRef.detectChanges();
	}

	public toggleCreateColor() {
		if (this.creatingColor) {
			this.disableCreateColor();
		} else {
			this.enableCreateColor();
		}
	}

	public onChooseColor(color: Color) {
		this.disableCreateColor();
		this.colorPickerInstance.lastColorHex = null;
		if (this.chosenColor && this.chosenColor.id !== color.id) {
			this.resetFormActions();
			this.resetColorActions(this.chosenColor);
			this.resetColorPickerItem(this.chosenColor);
		}
		this.nextAction(color);
		this.chosenColor = color;
		this.colorForm.patchValue({ name: color.name });
		this.chosenElementHex = color.hex;
		this.colorPickerInstance.setElementStyle(color.hex);
		this.colorPickerService.inputColorSubject.next(color.hex);
	}

	public async onSelect() {
		await this.adminWebService.updateImage(this.image.id, { color_id: this.chosenColor.id });
		this.image.color_id = this.chosenColor.id;
		this.image.color = this.chosenColor;
		this.dialogRef.close(this.chosenColor);
		this.cdRef.detectChanges();
	}

	public async onEdit() {
		if (!this.colorForm.valid) {
			return;
		}
		let hex = this.chosenElementHex;
		if (!hex) {
			hex = this.chosenColor.hex;
		}
		let params = { id: this.chosenColor.id, hex: hex, ...this.colorForm.value };
		await this.adminWebService.updateColor(params);
		this.chosenColor = await this.adminService.updateColor(params);
		this.resetFormActions();
		this.resetColorActions(this.chosenColor);
		this.onChooseColor(this.chosenColor);
	}

	public async onCreate() {
		if (!this.colorForm.valid) {
			return;
		}
		let params = { ...this.colorForm.value, hex: this.chosenElementHex };
		let resID = await this.adminWebService.createColor(params);
		this.chosenColor = await this.adminService.createColor({
			id: resID,
			...params
		});
		this.resetFormActions();
		this.resetColorActions(this.chosenColor);
		this.cdRef.detectChanges();
		this.colorPickerInstance.setColorItemElements();
		this.onChooseColor(this.chosenColor);
	}

	public async onDelete() {
		await this.adminWebService.deleteColor(this.chosenColor.id);
		this.adminService.deleteColor(this.chosenColor.id);
		this.chosenColor = null;
		this.resetFormActions();
		this.colorPickerInstance.setColorItemElements();
	}

	public cancel() {
		this.dialogRef.close();
	}

	private resetFormActions() {
		this.disableNameEdit();
		this.creatingColor = false;
		this.selectingColor = false;
		this.editingColor = false;
		this.deletingColor = false;
	}

	private enableCreateColor() {
		this.resetFormActions();
		if (this.chosenColor) {
			this.resetFormActions();
			this.resetColorActions(this.chosenColor);
			this.resetColorPickerItem(this.chosenColor);
			this.chosenColor = null;
		}
		this.enableNameEdit();
		this.colorForm.patchValue({ name: '' });
		this.creatingColor = true;
	}

	private disableCreateColor() {
		this.disableNameEdit();
		this.creatingColor = false;
	}

	private enableNameEdit() {
		this.colorForm.get('name').enable();
	}

	private disableNameEdit() {
		this.colorForm.get('name').disable();
	}

	private resetColorPickerItem(color: Color) {
		let index = this.colors.findIndex(c => c.id === color.id);
		this.colorPickerInstance.removeStyledElement(index);
		this.colorPickerInstance.setStyledElements();
		let el = this.colorPickerInstance.colorPickerItemElements[index];
		this.colorPickerInstance.setElementBackgroundColor(el, color.hex);
	}

	private resetColorActions(color: Color) {
		let index = this.colors.findIndex(c => c.id === color.id);
		if (index < 0) {
			return;
		}
		this.colors[index]['selected'] = false;
		this.colors[index]['edit'] = false;
		this.colors[index]['delete'] = false;
	}

	private nextAction(color: Color) {
		let index = this.colors.findIndex(c => c.id === color.id);
		if (this.selectingColor) {
			this.editColor(this.colors[index]);
		} else if (this.editingColor) {
			this.resetColorPickerItem(this.chosenColor);
			this.editingColor = false;
			this.deletingColor = true;
			this.colors[index]['edit'] = false;
			this.colors[index]['delete'] = true;
		} else if (this.deletingColor) {
			this.deletingColor = false;
			this.colors[index]['delete'] = false;
			this.chosenColor = null;
		} else {
			this.selectingColor = true;
			this.colors[index]['selected'] = true;
			this.colorPickerInstance.addStyledElement(index);
		}

		this.cdRef.detectChanges();
	}

	private editColor(color) {
		this.selectingColor = false;
		this.editingColor = true;
		color['selected'] = false;
		color['edit'] = true;
		this.enableNameEdit();
	}

	ngOnDestroy(): void {
		if (this.chosenColor) {
			this.resetColorActions(this.chosenColor);
		}
		this.resetFormActions();
		this.colorSub.unsubscribe();
		this.colorPickerSub.unsubscribe();
	}
}
