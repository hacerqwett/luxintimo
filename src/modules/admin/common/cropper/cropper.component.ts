import { Component, Input, Output, EventEmitter } from '@angular/core';
import Cropper from 'zone-cropperjs';
import { UploaderService } from '../uploader/uploader.service';
import { Aspect } from '@utils/constants';

@Component({
	selector: 'cropper',
	templateUrl: './cropper.component.html',
})
export class CropperComponent {

	@Input() uri: any;
	
	@Output() close = new EventEmitter<any>();
	
	public curi: string;
	public loading: boolean = true;
	public aspect: number = 2;

	public cropper: Cropper;

	constructor(
		private uploaderService: UploaderService,
	) {
	}

	async ngOnInit() {
		await this.setCompressedDataURI(this.uri.value);
	}

	public onImageLoad() {
		let image: any = document.getElementById('image');

		this.cropper = new Cropper(image, {
			aspectRatio: .75,
			viewMode: 2,
			autoCropArea: 1,
			dragMode: 'move',
			zoomable: false,
			zoomOnTouch: false,
			zoomOnWheel: false,
			ready: () => {
				this.loading = false;
			}
		});
	}

	private async setCompressedDataURI(dataUri) {
		let image = new Image();
		image.src = dataUri;
		await new Promise((resolve, reject) => {
			image.onload = () => {
				resolve();
			};
		});

		let canvas = document.createElement("canvas");
		canvas.width = image.width;
		canvas.height = image.height;

		let ctx = canvas.getContext("2d");
		ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

		canvas.toBlob(blob => {
			this.uploaderService.blobToDataURL(blob, (dataUri) => this.curi = dataUri);
		}, "image/jpeg", 0.7);
	}

	crop() {
		this.loading = true;
		this.cropper.crop();
		this.cropper.getCroppedCanvas().toBlob((blob) => {
			this.uploaderService.inputDataSubject.next({ blob: blob, aspect: this.aspect });
		}, "image/jpeg", 0.7);
	}

	changeAspect() {
		if (this.aspect === Aspect.AspectRect75) {
			this.cropper.setAspectRatio(1);
			this.aspect = Aspect.AspectSquare;
		} else {
			this.cropper.setAspectRatio(.75);
			this.aspect = Aspect.AspectRect75;
		}
	}

	rotateLeft() {
		this.cropper.rotate(-90);
	}

	rotateRight() {
		this.cropper.rotate(90);
	}

	cancel() {
		this.uploaderService.inputDataSubject.next(null);
		this.close.emit(null);
	}
}
