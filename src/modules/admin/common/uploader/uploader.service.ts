import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Aspect } from '@utils/constants';

interface UploaderData {
	blob: Blob;
	aspect: Aspect;
}

@Injectable()
export class UploaderService {
		public inputDataSubject = new Subject<UploaderData>();
		
		public blobToDataURL(blob, callback) {
			var a = new FileReader();
			// @ts-ignore
			a.onload = function(e) {callback(e.target.result);}
			a.readAsDataURL(blob);
	}
}