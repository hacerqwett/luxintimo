import { Component, EventEmitter, Output, Input } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, UploaderOptions, UploadStatus } from 'ngx-uploader';
import { Auth } from '@utils/auth';
import { UploaderService } from './uploader.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'uploader',
	templateUrl: 'uploader.component.html'
})
export class UploaderComponent {
	options: UploaderOptions;
	formData: FormData;
	files: UploadFile[];
	uploadInput: EventEmitter<UploadInput>;
	dragOver: boolean;

	private uploaderSub: Subscription;

	@Input() uri: string;
	@Input() data: {};

	@Output() upload = new EventEmitter<any>();
	@Output() preview = new EventEmitter<any>();

	constructor(
		private uploaderService: UploaderService
	) {
		this.options = { concurrency: 1 };
		this.files = []; // local uploading files array
		this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
	}

	ngAfterViewInit(): void {
		this.uploaderSub = this.uploaderService.inputDataSubject.subscribe(data => {
			if (!data || !data.blob) {
				this.removeFile(this.files[this.files.length - 1].id);
				this.upload.emit(null);
				return;
			}
			let file = this.files[this.files.length - 1];
			file.nativeFile = new File([data.blob], file.name);
			this.data['aspect'] = data.aspect;
			this.startUpload();
		});
	}

	onUploadOutput(output: UploadOutput): void {
		switch (output.type) {
			case 'allAddedToQueue':
				// this.startUpload();
				break;
			case 'addedToQueue':
				if (typeof output.file !== 'undefined') {
					this.files.push(output.file);
					this.previewImage(output.file).then(response => {
						this.preview.emit({ value: response });
					});
				}
				break;
			case 'uploading':
				if (typeof output.file !== 'undefined') {
					// update current data in files array for uploading file
					const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
					this.files[index] = output.file;
				}
				break;
			case 'removed':
				// remove file from array when removed
				this.files = this.files.filter((file: UploadFile) => file !== output.file);
				break;
			case 'dragOver':
				this.dragOver = true;
				break;
			case 'dragOut':
			case 'drop':
				this.dragOver = false;
				break;
			case 'done':
				let unfinished = this.files.filter(file => !((file.progress.status === UploadStatus.Done)));
				if (unfinished.length > 0) {
					return
				}

				this.files.forEach(file => {
					this.upload.emit(file.response);
				});

				this.files = [];
		}
	}

	previewImage(file: any) {
		const fileReader = new FileReader();
		return new Promise(resolve => {
			fileReader.readAsDataURL(file.nativeFile);
			fileReader.onload = function (e: any) {
				resolve(e.target.result);
			}
		});
	}

	startUpload(): void {
		let token = Auth.getJWT();
		const event: UploadInput = {
			type: 'uploadAll',
			url: this.uri,
			method: 'POST',
			headers: { 'Authorization': 'Bearer ' + token },
			data: { data: JSON.stringify(this.data) },
			includeWebKitFormBoundary: true
		};
		this.uploadInput.emit(event);
	}

	cancelUpload(id: string): void {
		this.uploadInput.emit({ type: 'cancel', id: id });
	}

	removeFile(id: string): void {
		this.uploadInput.emit({ type: 'remove', id: id });
	}

	removeAllFiles(): void {
		this.uploadInput.emit({ type: 'removeAll' });
	}

	ngOnDestroy(): void {
		this.uploaderSub.unsubscribe();
	}
}