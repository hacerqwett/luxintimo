import { Component, Input, NgZone, ChangeDetectorRef } from '@angular/core';
import Siema from 'ng-siema';
import { CarouselService } from './carouse.service';
import { Image, ImageSize } from '@models/image';
import { Aspect } from '@utils/constants';

@Component({
	selector: 'carousel',
	templateUrl: './carousel.component.html',
})
export class CarouselComponent {

	private _lastImages: Image[] = [];
	private _images: Image[] = [];
	@Input() set images(value: Image[]) {
		this._images = value;
		if (!this.siema) {
			return;
		}
		
		let imagesToAdd: Image[] = this._images.filter(i => !this._lastImages.find(li => li === i));
		let indexToRemove: number[] = this._lastImages.map((li, index) => index).filter(index => !this._images.find(i => i === this._lastImages[index]));
		
		imagesToAdd.forEach(image => {
			let el = this.createElement(image);
			this.siema.append(el);
			this.siema.next();
		});
		
		// Append placeholder
		if (this._images.length === 0 && !this.placeholder) {
			let el = this.createPlaceholder();
			this.siema.append(el);
			this.siema.next();
			this.placeholder = true;
		}
		
		indexToRemove.forEach(index => {
			this.siema.next();
			this.siema.remove(index);
		});
		
		// Remove placeholder
		if (this._images.length > 0 && this._lastImages.length === 0 && this.placeholder) {
			this.siema.next();
			this.siema.remove(0);
			this.placeholder = false;
		}
		
		this._lastImages = this._images;
		this.carouselService.slideSubject.next(this.getCurrentSlide());
		this.cdRef.detectChanges();
	}

	get images(): Image[] {
		return this._images;
	}
	
	private placeholder: boolean = false;

	public currentSlide: number;
	
	private siema: Siema;
	private options = {
		selector: '.siema',
		duration: 300,
		easing: 'ease-out',
		perPage: 1,
		startIndex: 0,
		draggable: true,
		multipleDrag: false,
		threshold: 20,
		loop: false,
		rtl: false,
		onInit: () => { },
		onChange: () => {
			this.ngZone.run(() => {
				this.carouselService.slideSubject.next(this.getCurrentSlide());
			});
		},
	};

	constructor(
		private carouselService: CarouselService,
		private ngZone: NgZone,
		private cdRef: ChangeDetectorRef,
	) {
	}

	ngAfterViewInit(): void {
		this.siema = new Siema(this.options);
		this.images = this._images;
	}

	private createElement(image: Image): HTMLElement {
		let parent = document.createElement('div');
		if (image.aspect === Aspect.AspectSquare) {
			parent.classList.value = 'w-100 pt-100 position-relative';
		} else {
			parent.classList.value = 'w-100 pt-125 position-relative';
		}

		let child = document.createElement('img');
		child.classList.value = 'position-absolute l-0 t-0 w-100 h-100 obj-cover';
		child.src = image.uri;
		child.srcset = image.genSrcSet(ImageSize.Medium);

		parent.appendChild(child);

		return parent;
	}

	private createPlaceholder(): HTMLElement {
		const newElement = document.createElement('div');
		newElement.classList.value = 'border-dashed pt-100 mx-3';
		return newElement;
	}

	private getCurrentSlide(): number {
		if (this.siema.currentSlide >= this.images.length) {
			this.currentSlide = this.images.length - 1;
		} else {
			this.currentSlide = this.siema.currentSlide;
		}
		return this.currentSlide;
	}

	ngOnDestroy(): void {
		this.siema.destroy();
	}
}
