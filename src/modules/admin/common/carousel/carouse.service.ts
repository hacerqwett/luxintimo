import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class CarouselService {
	private _count = 1;
	public get count() {
		return this._count++;
	}
	
	public slideSubject = new ReplaySubject<number>(1);
}