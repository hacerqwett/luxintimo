import { Injectable } from '@angular/core';
import { SubCategory, Category, FullCategory } from '@models/category';
import { Manufacturer } from '@models/manufacturer';
import { AppService } from '@modules/app/app.service';
import { Color } from '@models/color';
import { first } from 'rxjs/operators';


@Injectable()
export class AdminService {
	constructor(
		private appService: AppService
	) { }

	public async createManufacturer(json): Promise<Manufacturer> {
		let manufacturer = new Manufacturer().fromJSON(json);
		let manufacturers = await this.appService.manufacturerSubject.pipe(first()).toPromise();
		manufacturers.push(manufacturer);
		this.appService.manufacturerSubject.next(manufacturers);

		return manufacturer;
	}

	public async updateManufacturer(json): Promise<Manufacturer> {
		let updated = new Manufacturer().fromJSON(json);
		let manufacturers = await this.appService.manufacturerSubject.pipe(first()).toPromise();;
		let i = manufacturers.findIndex(c => c.id === updated.id);
		manufacturers[i] = updated;
		this.appService.manufacturerSubject.next(manufacturers);

		return updated;
	}

	public async createCategory(json): Promise<Category> {
		let category = new Category().fromJSON(json);
		let categories = await this.appService.categorySubject.pipe(first()).toPromise();
		categories.push(category);
		this.appService.categorySubject.next(categories);

		this.createFullCategory(category);
		return category;
	}

	public async updateCategory(json): Promise<Category> {
		let updated = new Category().fromJSON(json);
		let categories = await this.appService.categorySubject.pipe(first()).toPromise();
		let i = categories.findIndex(c => c.id === updated.id);
		categories[i] = updated;
		this.appService.categorySubject.next(categories);
		this.updateFullCategory(updated);

		return updated;
	}

	public async createSubCategory(json): Promise<SubCategory> {
		let subCategory = new SubCategory().fromJSON(json);
		let subCategories = await this.appService.subCategorySubject.pipe(first()).toPromise();
		subCategories.push(subCategory);
		this.appService.subCategorySubject.next(subCategories);

		let categories = await this.appService.categorySubject.pipe(first()).toPromise();
		let category = categories.find(c => c.id === subCategory.category_id)
		this.updateFullCategory(category, subCategory);

		return subCategory;
	}

	public async updateSubCategory(json): Promise<SubCategory> {
		let updated = new SubCategory().fromJSON(json);
		let subCategories = await this.appService.subCategorySubject.pipe(first()).toPromise();
		let i = subCategories.findIndex(c => c.id === updated.id);
		subCategories[i] = updated;
		this.appService.subCategorySubject.next(subCategories);

		let categories = await this.appService.categorySubject.pipe(first()).toPromise();;
		let category = categories.find(c => c.id === updated.category_id)
		this.updateFullCategory(category, updated);

		return updated;
	}

	private async createFullCategory(category: Category): Promise<void> {
		let fullCategories = await this.appService.fullCategorySubject.pipe(first()).toPromise();
		fullCategories.push(new FullCategory(category, []));
		this.appService.fullCategorySubject.next(fullCategories);
	}

	private async updateFullCategory(category: Category, subCategory?: SubCategory) {
		let fullCategories = await this.appService.fullCategorySubject.pipe(first()).toPromise();;
		let i = fullCategories.findIndex(fc => fc.category.id === category.id);
		fullCategories[i].category = category;
		if (subCategory) {
			fullCategories[i].subcategories.push(subCategory);
		}
		this.appService.fullCategorySubject.next(fullCategories);
	}

	public async createColor(json): Promise<Color> {
		let color = new Color().fromJSON(json);
		let colors = await this.appService.colorSubject.pipe(first()).toPromise();;
		colors = [color, ...colors];
		this.appService.colorSubject.next(colors);

		return color;
	}

	public async updateColor(json): Promise<Color> {
		let updated = new Color().fromJSON(json);
		let colors = await this.appService.colorSubject.pipe(first()).toPromise();
		let i = colors.findIndex(c => c.id === updated.id);
		colors[i] = updated;
		this.appService.colorSubject.next(colors);

		return updated;
	}

	public async deleteColor(id: number): Promise<void> {
		let colors = await this.appService.colorSubject.pipe(first()).toPromise();
		let i = colors.findIndex(c => c.id === id);
		colors.splice(i, 1);
		this.appService.colorSubject.next(colors);
	}
}