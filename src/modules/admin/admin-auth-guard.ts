import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AppService } from '@modules/app/app.service';
import { map } from 'rxjs/operators';

@Injectable()
export class AdminAuthGuard implements CanActivate {

  constructor(protected appService: AppService,
    protected router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
			return this.appService.userSubject.asObservable().pipe(map(user => {
				if (user && user.admin) {
					return true;
				} else {
					this.router.navigate(["login"]);
					return false;
				}
			}));
  }
}