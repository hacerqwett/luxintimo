import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { AdminWebService } from '@providers/web.service';
import { Subscription, Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Image } from '@models/image';
import { Size } from '@models/size';
import { CarouselService } from '@modules/admin/common/carousel/carouse.service';
import { ColorPickerModalComponent } from '@modules/admin/common/modals/color-picker-modal/color-picker-modal.component';
import { SizeModalComponent } from '@modules/admin/common/modals/size-modal/size-modal.component';
import { Utils } from '@utils/utils';
import { environment } from '@env/environment';
import { AppService } from '@modules/app/app.service';
import { Color } from '@models/color';
import { FullClothing } from '@models/clothing';
import { Status } from '@utils/constants';

@Component({
	selector: 'clothing-top',
	templateUrl: './clothing-top.component.html',
})
export class ClothingTopComponent {

	public uploadData: any = {};

	public status = Status.Draft;

	private _fc: FullClothing;
	@Input() set fullClothing(fc: FullClothing) {
		if (!fc) return;

		this._fc = fc;
		this.uploadData = { clothing_id:  this._fc.id };
		this.images = this._fc.images;
		this.currentImage = this.images[0];
		this.sizes = this._fc.sizes;
		this.setImagesColor();
	}

	@Input() onSave: Subject<void>;
	
	public images: Image[] = [];
	public sizes: Size[] = []
	
	public currentImage: Image;
	private carouselSub: Subscription;

	private colorSub: Subscription;
	private colors: Color[] = [];

	public uploadUri: string = environment.uploadImageWebServiceUri;
	
	public preview: boolean = false;
	public previewUri: any;

	public loading: boolean = false;

	constructor(
		private appService: AppService,
		private adminWebService: AdminWebService,
		private dialog: MatDialog,
		private carouselService: CarouselService,
	) {
	}
	
	ngOnInit(): void {
		this.carouselSub = this.carouselService.slideSubject.subscribe((slide) => {
			this.currentImage = this.images[slide] || undefined;
		});
		this.colorSub = this.appService.colorSubject.subscribe(colors => {
			this.colors = colors;
			this.setImagesColor();
		});

	}

	public openColorPickerModal() {
		let last_color_id = this.currentImage.color_id;

		const dialogRef = this.dialog.open(ColorPickerModalComponent, {
			width: '420px',
			data: { image: this.currentImage },
		});

		let sub = dialogRef.afterClosed().subscribe(color => {
			if (color) {

				this.removeSizeColorIfLast(last_color_id);
				this.currentImage.color = color;
				this.currentImage.color_id = color.id;
			}

			sub.unsubscribe();
		});
	}

	private setImagesColor() {
		this.images.forEach(i => {
			if (!i.color) {
				i.color = this.colors.find(c => c.id === i.color_id);
			}
		});
	}

	private removeSizeColorIfLast(last_color_id) {
		if (last_color_id > 0 && this.images.filter(img => last_color_id === img.color_id).length === 0) {
			this.sizes = this.sizes.filter(s => s.color_id !== last_color_id);
		}
	}

	public openSizeModal() {
		let colors = [];
		this.images.forEach(img => {
			if (img.color) {
				colors.push(img.color);
			}
		});
		colors = Utils.getOnlyUnique(colors);
		this.dialog.open(SizeModalComponent, {
			width: '450px',
			data: { colors: colors, sizes: this.sizes, clothing_id: this._fc.id },
		});
	}

	public async deleteImage() {
		this.loading = true;
		try {
			await this.adminWebService.deleteImage(this.currentImage.id);
			this.images = this.images.filter(img => img.id !== this.currentImage.id);

			if (this.sizes.length > 0 && this.images.filter(img => this.currentImage.color_id === img.color_id).length === 0) {
				this.sizes = this.sizes.filter(s => s.color_id !== this.currentImage.color_id);
			}

			if (this.images.length === 0) {
				this.currentImage = null;
			}
		} finally {
			this.loading = false;
		}
	}

	public onPreviewClose() {
		this.preview = false;
	}

	public onPreview(uri) {
		this.previewUri = uri;
		this.preview = true;
	}

	public onUpload(res: Image) {
		this.preview = false;

		if (!res) {
			return;
		}

		let image = new Image().fromJSON({ ...res, clothing_id: this._fc.id });
		this.images = [...this.images, image];
		this.currentImage = image;
		this.previewUri = null;
	}

	ngOnDestroy(): void {
		this.colorSub.unsubscribe();
		this.carouselSub.unsubscribe();
	}
}
