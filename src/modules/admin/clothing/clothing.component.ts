import { Component } from '@angular/core';
import { AdminWebService, ClothingWebService } from '@providers/web.service';
import { Subscription, Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Status } from '@utils/constants';
import { Utils } from '@utils/utils';
import { AppService } from '@modules/app/app.service';
import { LuxCurrencyPipe } from '@pipes/currency-pipe';
import { FullClothing } from '@models/clothing';
import {Location} from '@angular/common';


@Component({
	selector: 'app-clothing',
	templateUrl: './clothing.component.html',
})
export class ClothingComponent {

	public fullClothing: FullClothing;

	public clothingForm: FormGroup = new FormGroup({
		id: new FormControl(null, Validators.required),
		name: new FormControl(null, Validators.required),
		manufacturer_id: new FormControl(null, Validators.required),
		category_id: new FormControl(null, Validators.required),
		subcategory_id: new FormControl(null, Validators.required),
		status: new FormControl(Status.Draft, Validators.required),
		price: new FormControl(0, [Validators.required, Validators.min(0.01)]),
		has_discount: new FormControl(false, Validators.required),
		discount: new FormControl(0, Validators.required),
		discount_percentage: new FormControl(0, Validators.required),
		description: new FormControl(null, Validators.required),
	});

	public onSave = new Subject<Status>();

	private currencyPipe = new LuxCurrencyPipe();

	private routeSub: Subscription;

	public loading: boolean = false;

	constructor(
		private adminWebService: AdminWebService,
		private clothingWebService: ClothingWebService,
		private appService: AppService,
		private route: ActivatedRoute,
		private router: Router,
		private location: Location
	) {
		this.routeSub = this.route.paramMap.subscribe(params => {
			this.getClothing(params.get("id"));
		});

		this.onSave.subscribe((status) => this.saveClothing(status));
	}
	
	public async saveClothing(status: Status) {
		this.loading = true;
		if (status) {
			this.clothingForm.patchValue({ status: status });
		}
		try {
			if (this.fullClothing) {
				await this.updateClothing();
			} else {
				await this.createClothing();
			}
		} finally {
			this.loading = false;
		}
	}
	
	private async createClothing() {
		let id = await this.adminWebService.createClothing(this.getParams());
		this.clothingForm.patchValue({ id: id });
		this.location.replaceState(`/admin/clothing/${id}`);
		this.getClothing(id);
	}

	private async updateClothing() {
		await this.adminWebService.updateClothing(this.getParams());
	}

	private getParams() {
		let params = Utils.getValidFields(this.clothingForm.value);
		if (params.hasOwnProperty('price')) {
			params['price'] = parseFloat(params['price']);
		}
		if (params.hasOwnProperty('discount')) {
			params['discount'] = params['has_discount'] ? parseFloat(params['discount']) : null;
		}
		return params;
	}

	private async getClothing(id) {
		if (!id) {
			this.reset();
			return;
		}

		try {
			let res = await this.clothingWebService.getClothing(id);
			this.fullClothing = await this.appService.buildFullClothing(res.clothing, res.details);
			let price = this.currencyPipe.transform('' + this.fullClothing.price, ' лв.');
			let discount = this.currencyPipe.transform('' + this.fullClothing.discount, ' лв.');
			let has_discount = res.clothing.discount;
			let category_id = this.fullClothing.category ? this.fullClothing.category.id : null;
			this.clothingForm.patchValue({...this.fullClothing, ...this.fullClothing.details, category_id, price, discount, has_discount});
		} catch {
			this.reset();
			this.router.navigate(['admin', 'clothing']);
		}
	}

	private reset() {
		this.clothingForm.reset();
		this.clothingForm.patchValue({ price: 0, discount: 0, discount_percentage: 0, status: Status.Draft });
		this.fullClothing = null;
	}

	ngOnDestroy(): void {
		this.routeSub.unsubscribe();
	}
}
