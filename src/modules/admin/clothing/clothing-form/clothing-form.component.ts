import { Component, Input } from '@angular/core';
import { AdminWebService } from '@providers/web.service';
import { Subscription, Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Category, FullCategory, SubCategory } from '@models/category';
import { Manufacturer } from '@models/manufacturer';
import { AppService } from '@modules/app/app.service';
import { Status } from '@utils/constants';
import { CreateManufacturerModalComponent } from '@modules/admin/common/modals/create-manufacturer/create-manufacturer.component';
import { CreateCategoryModalComponent } from '@modules/admin/common/modals/create-category/create-category.component';
import { CreateSubCategoryModalComponent } from '@modules/admin/common/modals/create-subcategory/create-subcategory.component';
import { LuxCurrencyPipe } from '@pipes/currency-pipe';

@Component({
	selector: 'clothing-form',
	templateUrl: './clothing-form.component.html',
})
export class ClothingFormComponent {

	@Input() clothingForm: FormGroup;
	@Input() onSave: Subject<Status>;

	public loading: boolean = false;

	public status = Status;
	public formStatus = null;

	public categories: Category[] = [];
	private categorySub: Subscription;

	public fullCategories: FullCategory[] = [];
	private fullCategorySub: Subscription;

	private categoryFormSub: Subscription;
	public subCategories: SubCategory[] = [];

	public manufacturers: Manufacturer[] = [];
	private manufacturersSub: Subscription;

	private priceSub: Subscription;
	private discountPriseSub: Subscription;
	private discountPercentageSub: Subscription;

	constructor(
		private appService: AppService,
		private dialog: MatDialog,
		private currencyPipe: LuxCurrencyPipe
	) {
	}

	ngOnInit(): void {
		this.fullCategorySub = this.appService.fullCategorySubject.subscribe(fullCategories => this.fullCategories = fullCategories);
		this.categorySub = this.appService.categorySubject.subscribe(categories => {
			this.categories = categories;
		});
		this.manufacturersSub = this.appService.manufacturerSubject.subscribe(manufacturers => {
			this.manufacturers = manufacturers;
		});
		this.categoryFormSub = this.clothingForm.get('category_id').valueChanges.subscribe((category_id) => {
			let fc = this.fullCategories.find(fc => fc.category.id === category_id);
			if (fc && fc.subcategories) {
				this.subCategories = fc.subcategories;
			}
		});

		this.priceSub = this.clothingForm.get('price').valueChanges.subscribe(() => this.onPriceChange('price'));
		this.discountPriseSub = this.clothingForm.get('discount').valueChanges.subscribe(() => this.onPriceChange('discount'));
		this.discountPercentageSub = this.clothingForm.get('discount_percentage').valueChanges.subscribe(() => this.onDiscountPercentageChange());
	}
	
	private onPriceChange(field) {
		let price_string: string = this.clothingForm.get('price').value;
		let discount_string: string = this.clothingForm.get('discount').value;

		let price_float = this.getFloatValue(price_string);
		let discount_float = this.getFloatValue(discount_string);

		let increase = price_float - discount_float;
		let percentage = increase / price_float * 100;
		let discount_percentage = '' + Math.round(!isNaN(percentage) && isFinite(percentage) ? percentage : 0);
		if (this.clothingForm.get('has_discount').value && Number(price_string) || Number(discount_string)) {
			discount_percentage = this.currencyPipe.transform(discount_percentage, '%', 0)
		}
		let patch = { discount_percentage };
		let regex = /.*?\.\d{0,2}$/;
		if (field === 'price') {
			patch['price'] = regex.test(price_string) ? price_string : ('' + price_float);
			patch['discount'] = this.currencyPipe.transform('' + discount_float, ' лв.');
		} else {
			patch['discount'] = regex.test(discount_string) ? discount_string : ('' + discount_float);
		}
		this.clothingForm.patchValue(patch, {emitEvent: false, onlySelf: true});
		this.validateDiscount(price_float, discount_float);
	}

	public onDiscountPercentageChange() {
		let percentage = this.clothingForm.get('discount_percentage').value;
		let price_float = this.getFloatValue(this.clothingForm.get('price').value);
		let discount_float = price_float - price_float * percentage / 100;
		this.clothingForm.patchValue({ 
			discount: this.currencyPipe.transform('' + discount_float, ' лв.'),
			discount_percentage: '' + this.getFloatValue(percentage)},
			{emitEvent: false, onlySelf: true});
		this.validateDiscount(price_float, discount_float);
	}

	private getFloatValue(val: string) {
		if (val === '0' || val === '') return 0;
		return parseFloat(val);
	}

	private validateDiscount(orig, disc) {
		this.clothingForm.get('discount').setErrors(disc < 0 || orig <= disc ? {'incorrect': true} : null);
	}

	public onlyNumbers(event) {
		let charCode = (event.query) ? event.query : event.keyCode;
		if (charCode !== 46 && (charCode > 31 && (charCode < 48 || charCode > 57))) {
			return false;
		}

		return true;
	}

	public openManufacturerModal(): void {
		const dialogRef = this.dialog.open(CreateManufacturerModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(manufacturer => {
			if (manufacturer) {
				this.clothingForm.patchValue({ manufacturer_id: manufacturer.id });
			}
		});
	}

	public openCategoryModal() {
		const dialogRef = this.dialog.open(CreateCategoryModalComponent, {
			width: '450px',
		});

		dialogRef.afterClosed().subscribe(category => {
			if (category) {
				this.clothingForm.patchValue({ category_id: category.id });
			}
		});
	}

	public openSubCategoryModal() {
		const dialogRef = this.dialog.open(CreateSubCategoryModalComponent, {
			width: '450px',
			data: { category_id: this.clothingForm.get('category_id').value },
		});

		let sub = dialogRef.afterClosed().subscribe(subCategory => {
			if (subCategory) {
				this.clothingForm.patchValue({
					category_id: subCategory.category_id,
					subcategory_id: subCategory.id
				});
			}

			sub.unsubscribe();
		});
	}

	ngOnDestroy(): void {
		this.fullCategorySub.unsubscribe();
		this.categorySub.unsubscribe();
		this.manufacturersSub.unsubscribe();
		this.categoryFormSub.unsubscribe();
		this.priceSub.unsubscribe();
		this.discountPriseSub.unsubscribe();
		this.discountPercentageSub.unsubscribe();
	}
}
