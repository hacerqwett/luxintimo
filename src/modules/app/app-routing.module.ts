import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { CollectionComponent } from './collection/collection.component';
import { AdminAuthGuard } from '@modules/admin/admin-auth-guard';
import { ClothingComponent } from './clothing/clothing.component';

const routes: Routes = [
	{ path: '', redirectTo: 'collection', pathMatch: 'full' },
	{ path: 'login', component: LoginComponent },
	{ path: 'admin', canActivate: [AdminAuthGuard], loadChildren: '@modules/admin/admin.module#AdminModule' },
	{ path: 'collection', component: CollectionComponent },
	{ path: 'clothing/:id', component: ClothingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
