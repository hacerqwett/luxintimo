import { Component, ElementRef, Input } from '@angular/core';
import { LuxDropdownService } from './lux-dropdown.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'lux-dropdown',
	templateUrl: './lux-dropdown.component.html',
})
export class LuxDropdownComponent {

	private dropdownSub: Subscription;

	constructor(
		private container: ElementRef,
		private dropdownService: LuxDropdownService
	) {
	}

	@Input() title: string;
	@Input() id: string;

	private input: HTMLInputElement;

	ngAfterViewInit(): void {
		this.input = this.container.nativeElement.querySelectorAll('.lux-dropdown > input')[0];
		// @ts-ignore
		this.input.__zone_symbol__addEventListener("change", () => this.onDropdownClick(this.input));

		this.dropdownSub = this.dropdownService.toggleDropdownSubject.subscribe((id) => {
			if (id !== this.id) {
				return;
			}
			this.input.checked = false;
			var event = new Event('change');
			this.input.dispatchEvent(event);
		})
	}

	private onDropdownClick(input: HTMLInputElement) {
		//@ts-ignore
		__zone_symbol__requestAnimationFrame(() => {
			let ul: HTMLElement = input.parentElement.getElementsByTagName('ul')[0];
			if (input.checked) {
				let height = 0;
				for (let i = 0; i < ul.children.length; ++i) {
					height += ul.children[i].clientHeight;
				}
				ul.style.maxHeight = height + 'px';
			} else {
				ul.style.maxHeight = 0 + 'px';
			}
		});
	}

	ngOnDestroy(): void {
		this.dropdownSub.unsubscribe();
	}
}
