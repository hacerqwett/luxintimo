import { Component, Input } from '@angular/core';
import { Clothing } from '@models/clothing';

@Component({
  selector: 'lux-stars',
  templateUrl: './lux-stars.component.html',
})
export class LuxStarsComponent {

	@Input() public clothing: Clothing;

	public stars: Array<number> = [];
	public halfStar: boolean;
	public emptyStars: Array<number> = [];
	
  constructor(
	) { }
	
	ngOnInit(): void {
		this.stars = Array(Number(this.clothing.rating.toFixed(0)) - 1).fill(0);
		this.halfStar = (this.clothing.rating - this.stars.length) < 0.7;
		this.emptyStars = Array(5 - this.stars.length - 1).fill(0);
	}
}
