import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'lux-spinner',
	templateUrl: './lux-spinner.component.html',
	styles: [ 'lux-spinner { display: block; width: 100%; height: 100%; background: #ffffffb0; position: absolute; left: 0; top: 0; }' ],
	encapsulation: ViewEncapsulation.None,
})
export class LuxSpinnerComponent {
  constructor(
  ) { }
}
