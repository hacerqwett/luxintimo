import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CollectionComponent } from '../collection/collection.component';
import { LuxCurrencyDirective, LuxCurrencyPipe } from '@pipes/currency-pipe';
import { ClothingCardComponent } from '../collection/clothing-card/clothing-card.component';
import { LuxFilterComponent } from './lux-filter/lux-filter.component';
import { RouterModule } from '@angular/router';
import { LuxStarsComponent } from './lux-stars/lux-stars.component';
import { LuxDropdownComponent } from './lux-dropdown/lux-dropdown.component';
import { LuxMenuComponent } from './lux-menu/lux-menu.component';
import { ClothingComponent } from '../clothing/clothing.component';
import { CarouselComponent } from '@modules/admin/common/carousel/carousel.component';

@NgModule({
	declarations: [
		CollectionComponent,
		ClothingCardComponent,
		LuxFilterComponent,
		LuxStarsComponent,
		LuxCurrencyDirective,
		LuxCurrencyPipe,
		LuxDropdownComponent,
		LuxMenuComponent,
		ClothingComponent,
		CarouselComponent
	],
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
	],
	exports: [
		FormsModule,
		CollectionComponent,
		LuxCurrencyDirective,
		LuxCurrencyPipe,
		LuxFilterComponent,
		LuxStarsComponent,
		LuxDropdownComponent,
		LuxMenuComponent,
		ClothingComponent,
		CarouselComponent
	]
})
export class LuxCommonModule { }
