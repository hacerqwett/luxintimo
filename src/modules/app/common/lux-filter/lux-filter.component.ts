import { Component } from '@angular/core';
import { Category, SubCategory } from '@models/category';
import { Subscription } from 'rxjs';
import { AppService } from '@modules/app/app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SIZES, LuxEnum } from '@utils/constants';
import { Color } from '@models/color';
import { Manufacturer } from '@models/manufacturer';
import { LuxDropdownService } from '../lux-dropdown/lux-dropdown.service';
import { FilterParams } from '@models/filter';
import { Utils } from '@utils/utils';

@Component({
	selector: 'lux-filter',
	templateUrl: './lux-filter.component.html',
})
export class LuxFilterComponent {

	public categories: Category[] = [];
	public selectedCategory: Category;

	private _subCategories: SubCategory[];
	public subCategories: SubCategory[];

	public colors: Color[];
	public manufacturers: Manufacturer[];

	public sizes = SIZES;

	public minPrice: number;
	public maxPrice: number;

	private filterParams: FilterParams = new FilterParams();

	private categorySub: Subscription;
	private subCatSub: Subscription;
	private colorSub: Subscription;
	private querySub: Subscription;
	private manuSub: Subscription;

	constructor(
		private appService: AppService,
		private route: ActivatedRoute,
		private router: Router,
		private luxDropdownService: LuxDropdownService
	) {
		this.categorySub = this.appService.categorySubject.subscribe(categories => {
			this.categories = categories;
		});

		this.colorSub = this.appService.colorSubject.subscribe(colors => {
			this.colors = Utils.getOnlyUnique(colors, 'name');
		});

		this.subCatSub = this.appService.subCategorySubject.subscribe(subCategories => {
			this._subCategories = subCategories;
			this.filterFromParams(this.filterParams);
		});

		this.querySub = this.route.queryParamMap.subscribe(params => {
			this.filterParams = new FilterParams(JSON.parse(JSON.stringify(params['params'])));
			this.filterFromParams(this.filterParams);
		});

		this.manuSub = this.appService.manufacturerSubject.subscribe(manufacturers => {
			this.manufacturers = manufacturers;
		});

		this.navigate();
	}

	selectCategory(category: Category) {
		this.toggleCategoryAsSelected(category);
		this.filterSubCategories(category.id);
		this.setCategoryParams(category.id);
		this.navigate(this.filterParams);
	}

	selectSubCategory(subCategory: SubCategory) {
		this.setParams('subcat', subCategory.id);
		this.navigate(this.filterParams);
	}

	selectSize(size: LuxEnum) {
		this.setParams('sizes', size.id);
		this.navigate(this.filterParams);
	}

	selectColor(color: Color) {
		this.setParams('colors', color.id);
		this.navigate(this.filterParams);
	}

	selectManufacturer(manufacturer: Manufacturer) {
		this.setParams('brands', manufacturer.id);
		this.navigate(this.filterParams);
	}

	setPrice() {
		if (!this.maxPrice && !this.minPrice && (this.filterParams['minPrice'] || this.filterParams['maxPrice'])) {
			this.filterParams['minPrice'] = null;
			this.filterParams['maxPrice'] = null;
			this.navigate(this.filterParams);
			return;
		}

		if ((!this.minPrice && !this.maxPrice) || this.minPrice > this.maxPrice) {
			// TODO: Add validation
			return;
		}
		
		this.filterParams['minPrice'] = this.minPrice;
		this.filterParams['maxPrice'] = this.maxPrice;

		if (!this.minPrice) {
			this.filterParams['minPrice'] = 0;
		}

		this.navigate(this.filterParams);
	}

	clearParams() {
		this.luxDropdownService.toggleDropdownSubject.next('filterDropdown');
		this.clearInputs();
		this.navigate(this.filterParams['gender']);
	}

	private clearInputs() {
		this.selectedCategory = null;
		this.categories.forEach(c => c['selected'] = false);
		if (this.subCategories) {
			this.subCategories.forEach(c => c['selected'] = false);
		}
		this.sizes.forEach(s => s['selected'] = false);
		this.manufacturers.forEach(m => m['selected'] = false);
		this.colors.forEach(c => c['selected'] = false);
		this.minPrice = null;
		this.maxPrice = null;
	}

	private setCategoryParams(categoryID) {
		if (this.selectedCategory) {
			this.filterParams['category'] = categoryID;
		} else {
			delete this.filterParams['category'];
		}
		if (this.subCategories) {
			this.subCategories.forEach(c => c['selected'] = false);
		}
		this.filterParams['subcat'] = null;
	}

	private setParams(prop, val) {
		if (!this.filterParams[prop]) {
			this.filterParams[prop] = [];
		}

		let index = this.filterParams[prop].findIndex(s => s == val);
		if (index >= 0) {
			this.filterParams[prop].splice(index, 1);
		} else {
			this.filterParams[prop].push(val);
		}
	}

	private navigate(params?) {
		if (params) {
			this.router.navigate([this.route.snapshot.url[0].path], { queryParams: this.filterParams });
		} else {
			this.router.navigate([this.route.snapshot.url[0].path]);
		}
	}

	private toggleCategoryAsSelected(category: Category) {
		if (this.selectedCategory) {
			this.selectedCategory['selected'] = false;
			if (category === this.selectedCategory) {
				this.selectedCategory = null;
				return;
			}
		}

		this.selectedCategory = category;
		category['selected'] = true;
	}

	private filterFromParams(params) {
		if (!params) {
			return;
		}

		let category = params['category'];
		this.filterSubCategories(category);
	}

	private filterSubCategories(categoryId) {
		if (categoryId && this._subCategories) {
			this.subCategories = this._subCategories.filter(sub => sub.category_id == categoryId);
		}
	}

	ngOnDestroy(): void {
		this.categorySub.unsubscribe();
		this.subCatSub.unsubscribe();
		this.colorSub.unsubscribe();
		this.querySub.unsubscribe();
		this.manuSub.unsubscribe();
	}
}
