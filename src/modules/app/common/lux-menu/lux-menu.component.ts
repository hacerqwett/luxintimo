import { Component, ElementRef } from '@angular/core';
import { LuxDropdownService } from '../lux-dropdown/lux-dropdown.service';

@Component({
	selector: 'lux-menu',
	templateUrl: './lux-menu.component.html',
})
export class LuxMenuComponent {

	constructor(
		private container: ElementRef,
	) {
	}

	ngAfterViewInit(): void {
		let closeable = this.container.nativeElement.getElementsByClassName('lux-close');

		let menuInput = this.container.nativeElement.getElementsByClassName('menuInput')[0];

		menuInput.__zone_symbol__addEventListener("click", function () {
			if (this.checked) {
				return;
			}
		});

		for (let i = 0; i < closeable.length; i++) {
			let close = closeable[i];
			close.__zone_symbol__addEventListener("click", function () {
				menuInput.checked = false;
			});
		}
	}
}
