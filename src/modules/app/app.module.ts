import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app.component';
import { UserWebService, ClothingWebService, AdminWebService } from '@providers/web.service';
import { AppService } from './app.service';
import { TopBarComponent } from './top-bar/top-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LuxCommonModule } from './common/lux.common.module';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NotificationService } from '@providers/notification.service';
import { LuxCurrencyDirective, LuxCurrencyPipe } from '@pipes/currency-pipe';
import { AdminAuthGuard } from '@modules/admin/admin-auth-guard';
import { ResponsiveService } from '@providers/responsive.service';
import { LuxDropdownService } from './common/lux-dropdown/lux-dropdown.service';
import { CarouselService } from '@modules/admin/common/carousel/carouse.service';
import { AdminCommonModule } from '@modules/admin/common/admin.common.module';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
		LoginComponent,
  ],
  imports: [
		BrowserAnimationsModule,
		AppRoutingModule,
    LuxCommonModule,
		HttpClientModule,
  ],
  providers: [
    AppService,
		NotificationService,
    UserWebService,
    AdminWebService,
		ClothingWebService,
		LuxCurrencyDirective,
		LuxCurrencyPipe,
		{provide: LocationStrategy, useClass: HashLocationStrategy},
		AdminAuthGuard,
		ResponsiveService,
		LuxDropdownService,
		CarouselService
	],
  bootstrap: [AppComponent]
})
export class AppModule { }
