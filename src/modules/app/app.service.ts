import { Injectable } from '@angular/core';
import { Auth } from '@utils/auth';
import { User } from 'src/models/user';
import { ReplaySubject } from 'rxjs';
import { FullCategory, SubCategory, Category } from '@models/category';
import { ClothingWebService } from '@providers/web.service';
import { Manufacturer } from '@models/manufacturer';
import { Clothing, FullClothing, ClothingDetails } from '@models/clothing';
import { Color } from '@models/color';
import { Image } from '@models/image';
import { first } from 'rxjs/operators';

@Injectable()
export class AppService {
	public userSubject = new ReplaySubject<User>(1);
	public fullCategorySubject = new ReplaySubject<FullCategory[]>(1);
	public categorySubject = new ReplaySubject<Category[]>(1);
	public subCategorySubject = new ReplaySubject<SubCategory[]>(1);
	public manufacturerSubject = new ReplaySubject<Manufacturer[]>(1);
	public colorSubject = new ReplaySubject<Color[]>(1);
	
	constructor(
		private clothingWebService: ClothingWebService
	) {
		this.clothingWebService.getAll().then(res => {
			let categories = res.categories.map(cat => new Category().fromJSON(cat));
			let subCategories = res.subcategories.map(sub => new SubCategory().fromJSON(sub));
			this.categorySubject.next(categories);
			this.subCategorySubject.next(subCategories);
			this.manufacturerSubject.next(res.manufacturers.map(man => new Manufacturer().fromJSON(man)));
			this.colorSubject.next(res.colors.map(col => new Color().fromJSON(col)));

			this.createFullCategories(categories, subCategories);
		});

		this.initUser();
	}

	public setUser(user: User) {
		this.userSubject.next(user);
	}

	public login(user: User, token: string) {
		Auth.setJWT(token);
		Auth.setUser(user);

		this.setUser(user);
	}

	public logout() {
		Auth.clear();
		this.setUser(null);
	}

	private initUser() {
		if (!Auth.validJWT(Auth.getJWT())) {
			this.logout();
			return;
		}

		let userStr = Auth.getUser();
		let user = new User().fromJSON(JSON.parse(userStr));
		this.setUser(user);
	}

	private createFullCategories(categories: Category[], subCategories: SubCategory[]) {
		let fullCategories = [];
		categories.forEach(category => {
			let sub = subCategories.filter(sub => sub.category_id === category.id)
			let fc = new FullCategory(category, sub);
			fullCategories.push(fc);
		});
		this.fullCategorySubject.next(fullCategories);
	}


	public async buildFullClothing(clothing: Clothing, details?: ClothingDetails): Promise<FullClothing> {
		let manufacturer = null;
		if (clothing.manufacturer_id) {
			let manufacturers = await this.manufacturerSubject.pipe(first()).toPromise();
			manufacturer = manufacturers.find(m => m.id === clothing.manufacturer_id);
		}

		let subCategory = null;
		if (clothing.subcategory_id) {
			let subCategories = await this.subCategorySubject.pipe(first()).toPromise();
			subCategory = subCategories.find(sc => sc.id === clothing.subcategory_id);
		}

		let category = null;
		if (subCategory) {
			let categories = await this.categorySubject.pipe(first()).toPromise();
			category = categories.find(c => c.id === subCategory.category_id);
		}

		if (clothing.images && clothing.images.length > 0) {
			let colors = await this.colorSubject.pipe(first()).toPromise();
			for (let i = 0; i < clothing.images.length; ++i) {
				let image = clothing.images[i];
				if (image.color_id) {
					image.color = colors.find(c => c.id === image.color_id);
				}
			}
		}

		return new FullClothing().fromJSON({ ...clothing, manufacturer, category, subCategory, details });
	}
}