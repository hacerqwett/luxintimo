import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserWebService } from 'src/providers/web.service';
import { LoginResponse } from 'src/models/login-response';
import { AppService } from '@modules/app/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {

  public params = {};

  constructor(
    private webService: UserWebService,
    private appService: AppService,
    private router: Router
  ) { }

  public async onSubmit() {
    let res: LoginResponse = await this.webService.login(this.params);
    this.appService.login(res.user, res.token);
    this.router.navigate(['admin']);
  }
}
