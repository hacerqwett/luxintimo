import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ClothingWebService } from 'src/providers/web.service';
import { Clothing } from '@models/clothing';
import { ImageSize } from '@models/image';
import { AppService } from '../app.service';
import { ResponsiveService } from '@providers/responsive.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Resolution } from '@utils/constants';

@Component({
	selector: 'collection',
	templateUrl: './collection.component.html',
})
export class CollectionComponent {

	public clothing: Clothing[] = [];

	public isMobile: boolean = false;

	private index = 0;

	private resSub: Subscription;
	private querySub: Subscription;

	private initialized: boolean = false;

	constructor(
		private clothingWebService: ClothingWebService,
		private appService: AppService,
		private responsiveService: ResponsiveService,
		private route: ActivatedRoute,
		private router: Router,
		private cdRef: ChangeDetectorRef
	) {
		this.querySub = this.route.queryParamMap.subscribe(route => {
			this.initialized = true;
			let params = JSON.parse(JSON.stringify(route['params']));
			if (!params.gender) {
				params['gender'] = 1;
				this.router.navigate([this.route.snapshot.url[0].path], { queryParams: params });
				return;
			}
			
			this.setClothing(params);
		});

		if (!this.initialized) {
			this.router.navigate([this.route.snapshot.url[0].path], { queryParams: { gender: 1 } });
		}
	}

	ngAfterViewInit(): void {
		this.resSub = this.responsiveService.screenWidth.subscribe(width => {
			let curr = width <= Resolution.Medium;
			if (this.isMobile != curr) {
				this.isMobile = curr;
				this.cdRef.detectChanges();
			}
		});
	}

	private async setClothing(params?) {
		let clothing = await this.clothingWebService.getCollection(this.index, params);
		this.clothing = await Promise.all(clothing.map(async (c) => {
			let res = await this.appService.buildFullClothing(c)
			res.images.forEach(img => img.setSrcSet(ImageSize.Medium));
			return res;
		}));
		this.clothing.sort(function (a, b) {
			return a.id - b.id;
		});
	}

	ngOnDestroy(): void {
		this.resSub.unsubscribe();
		this.querySub.unsubscribe();
	}
}
