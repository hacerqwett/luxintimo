import { Component, Input } from '@angular/core';
import { FullClothing } from '@models/clothing';
import { Image } from '@models/image';
import { Color } from '@models/color';

@Component({
  selector: 'clothing-card',
  templateUrl: './clothing-card.component.html',
})
export class ClothingCardComponent {

	@Input() public clothing: FullClothing;

	public currImage: Image;
	public colors: Color[] = [];
	
  constructor(
	) { }
	
	ngOnInit(): void {
		this.currImage = this.clothing.images[0];
		this.colors = this.clothing.images.filter(c => c.color).map(i => i.color)
	}

	changeImage(color: Color) {
		let index = this.clothing.images.findIndex(i => i.color_id === color.id)
		if (index >= 0) {
			this.currImage = this.clothing.images[index];
		}
	}
}
