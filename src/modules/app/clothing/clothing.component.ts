import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { FullClothing } from '@models/clothing';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ClothingWebService } from '@providers/web.service';
import { AppService } from '../app.service';
import { Image } from '@models/image';
import { ResponsiveService } from '@providers/responsive.service';
import { Resolution } from '@utils/constants';

@Component({
  selector: 'clothing',
  templateUrl: './clothing.component.html',
})
export class ClothingComponent {

	public clothing: FullClothing;
	public currImage: Image;

	private routeSub: Subscription;
	private resSub: Subscription;

	public isMobile: boolean;

  constructor(
		private route: ActivatedRoute,
		private clothingWebService: ClothingWebService,
		private appService: AppService,
		private responsiveService: ResponsiveService,
		private cdRef: ChangeDetectorRef
	) {
		this.routeSub = this.route.paramMap.subscribe(params => {
			this.clothingWebService.getClothing(params.get("id")).then(res => {
				this.appService.buildFullClothing(res['clothing'], res['details']).then(fc => {
					this.clothing = fc;
					this.currImage = fc.images[0];
				});
			})
		});
	}
	
	ngOnInit(): void {
	}

	ngAfterViewInit(): void {
		this.resSub = this.responsiveService.screenWidth.subscribe(width => {
			let curr = width <= Resolution.ExtraSmall;
			if (this.isMobile != curr) {
				this.isMobile = curr;
				this.cdRef.detectChanges();
			}
		});
	}

	ngOnDestroy(): void {
		this.routeSub.unsubscribe();
		this.resSub.unsubscribe();
	}
}
