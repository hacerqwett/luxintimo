import { Component } from '@angular/core';
import { UserWebService } from 'src/providers/web.service';
import { ResponsiveService } from '@providers/responsive.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {

  constructor(
		private webService: UserWebService,
		private resposiveService: ResponsiveService
	) { }

	ngAfterViewInit(): void {
		// @ts-ignore
		window.__zone_symbol__addEventListener('resize', () => {
			this.resposiveService.checkWidth();
		}, true);
	}
}
