import { Component, ElementRef } from '@angular/core';
import { AppService } from '../app.service';
import { User } from '@models/user';
import { Router } from '@angular/router';

@Component({
	selector: 'top-bar',
	templateUrl: './top-bar.component.html',
})
export class TopBarComponent {

	public user: User;

	constructor(
		private appService: AppService,
		private router: Router,
		private container: ElementRef
	) {
		this.appService.userSubject.subscribe((user) => {
			this.user = user;
		});
	}

	ngAfterViewInit(): void {
		let closeable = this.container.nativeElement.getElementsByClassName('lux-close');

		let menuInput = this.container.nativeElement.getElementsByClassName('menuInput')[0];

		menuInput.__zone_symbol__addEventListener("click", function () {
			if (this.checked) {
				return;
			}
		});

		for (let i = 0; i < closeable.length; i++) {
			let close = closeable[i];
			close.__zone_symbol__addEventListener("click", function () {
				menuInput.checked = false;
			});
		}
	}

	public logout() {
		this.appService.logout();
		this.router.navigate(['login']);
	}
}
