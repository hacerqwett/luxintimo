const uri: string = 'http://192.168.0.105:8282'; 
export const environment = {
  production: false,
  webServiceUri: uri + '/api',
  uploadImageWebServiceUri: uri + '/api/image',
  imageWebServiceUri: uri + '/static',
};