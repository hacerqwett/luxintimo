// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const uri: string = 'http://10.115.5.44:8282'; 
export const environment = {
  production: false,
  webServiceUri: uri + '/api',
  uploadImageWebServiceUri: uri + '/api/image',
  imageWebServiceUri: uri + '/static',
};