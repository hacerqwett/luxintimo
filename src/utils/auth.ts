import jwt_decode from 'jwt-decode';
import { User } from 'src/models/user';

export class Auth {
	public static getJWT() {
		return localStorage.getItem('token');
	}

	public static setJWT(token: string): void {
		localStorage.setItem('token', token);
	}

	public static setUser(user: User): void {
		localStorage.setItem('user', JSON.stringify(user));
	}

	public static getUser(): string {
		return localStorage.getItem('user');
	}

	public static clear(): void {
		localStorage.removeItem('token');
		localStorage.removeItem('user');
	}

	public static validJWT(token): boolean {
		if (!token) return false;

		let decoded = jwt_decode(token);
		return decoded.exp > Math.round((new Date()).getTime() / 1000);
	}
}