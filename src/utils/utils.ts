import { isNullOrUndefined } from 'util';

export class Utils {
	public static getMap(obj: Array<any>, prop: string) {
		let map = {};
		obj.forEach((o) => map[prop] = o);
		return map;
	}

	public static getOnlyUnique(arr: Array<any>, prop?): Array<any> {
		if (prop) {
			const result = [];
			const map = new Map();
			for (let item of arr) {
				if(!map.has(item[prop])){
					map.set(item[prop], true);    // set any value to Map
					result.push(item);
				}
			}
			return result;
		} else {
			return arr.filter((value, index, self) => self.indexOf(value) === index);
		}
	}

	private static onlyUnique(value, index, self) {
		return self.indexOf(value) === index;
	}

	public static getValidFields(obj): any {
		let res = {};
		for (const field in obj) {
			if (obj.hasOwnProperty(field) && !isNullOrUndefined(obj[field])) {
				res[field] = obj[field];
			}
		}
		return res;
	}
}