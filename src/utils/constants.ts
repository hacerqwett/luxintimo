export interface LuxEnum {
	id: number,
	name: string
}

// Sizes - Size constants
export enum Size {
	XS = 1,
	S,
	M,
	L,
	XL,
	XXL,
	XXXL,
}

export var SIZES: LuxEnum[] = [
	{ id: Size.XS, name: Size[Size.XS] },
	{ id: Size.S, name: Size[Size.S] },
	{ id: Size.M, name: Size[Size.M] },
	{ id: Size.L, name: Size[Size.L] },
	{ id: Size.XL, name: Size[Size.XL] },
	{ id: Size.XXL, name: '2XL' },
	{ id: Size.XXXL, name: '3XL' },
];

export enum Gender {
	Male = 1,
	Female
}

export var GENDERS: LuxEnum[] = [
	{ id: Gender.Male, name: Gender[Gender.Male] },
	{ id: Gender.Female, name: Gender[Gender.Female] },
];

export var GENDERS_MAP = {};
GENDERS.forEach(g => GENDERS_MAP[g.id] = g.name);

export enum Status {
	Draft = 1,
	Live,
	Archived
}

export var STATUSES: LuxEnum[] = [
	{ id: Status.Draft, name: Status[Status.Draft] },
	{ id: Status.Live, name: Status[Status.Live] },
	{ id: Status.Archived, name: Status[Status.Archived] },
];

export enum Aspect {
	AspectSquare = 1,
	AspectRect75,
}

export var ASPECTS: LuxEnum[] = [
	{ id: Aspect.AspectSquare, name: Aspect[Aspect.AspectSquare] },
	{ id: Aspect.AspectRect75, name: Aspect[Aspect.AspectRect75] },
];

export enum Resolution {
	ExtraSmall = 1,
	Small,
	Medium,
	Large
}