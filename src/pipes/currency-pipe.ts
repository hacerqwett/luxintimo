import { Pipe, PipeTransform, Input } from '@angular/core';
import { Directive, HostListener, ElementRef, OnInit } from "@angular/core";

const PADDING = "000000";

@Pipe({
	name: 'luxCurrency'
})
export class LuxCurrencyPipe implements PipeTransform {

	private DECIMAL_SEPARATOR: string;
	private THOUSANDS_SEPARATOR: string;

	constructor() {
		// TODO comes from configuration settings
		this.DECIMAL_SEPARATOR = ".";
		this.THOUSANDS_SEPARATOR = ",";
	}

	transform(value: number | string, suffix: string, fractionSize = 2): string {
		if (value === "") return "";
		if (value === 0) value = '0';
		let [integer, fraction = ""] = (value || "").toString()
			.split(this.DECIMAL_SEPARATOR);

		fraction = fractionSize > 0
			? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
			: "";

		integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.THOUSANDS_SEPARATOR);

		return integer + fraction + suffix;
	}

	parse(value: string, suffix: string, fractionSize = 2): string {
		if (value === "") return "";
		let [integer, fraction = ""] = (value || "").split(this.DECIMAL_SEPARATOR);

		integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, "g"), "");

		fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
			? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
			: "";

		let res = integer + fraction;
		if (res.endsWith(suffix)) {
			res = (integer + fraction).slice(0, res.length - suffix.length);
		}
		return res;
	}
}

@Directive({ selector: "[lux-currency]" })
export class LuxCurrencyDirective implements OnInit {

	@Input() suffix: string;
	@Input() fractionSize: number = 2;

	private el: HTMLInputElement;

	constructor(
		private elementRef: ElementRef,
		private currencyPipe: LuxCurrencyPipe
	) {
		this.el = this.elementRef.nativeElement;
	}

	ngOnInit() {
		let value = this.currencyPipe.parse(this.el.value, this.suffix, this.fractionSize);
		this.el.value = this.currencyPipe.transform(value, this.suffix, this.fractionSize);
	}

	@HostListener("focus", ["$event.target.value"])
	onFocus(value) {
		this.el.value = this.currencyPipe.parse(value, this.suffix, this.fractionSize); // opossite of transform
	}

	@HostListener("blur", ["$event.target.value"])
	onBlur(value) {
		this.el.value = this.currencyPipe.transform(value, this.suffix, this.fractionSize);
	}
}